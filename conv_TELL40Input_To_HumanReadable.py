#!/bin/python
import numpy as np
import argparse
import warnings
from util import *

def PrepareHeader(Scenario):
    #setup things according to the scenario choosen
    bit_divs, bit_discrpts = 2*(None,)
    if Scenario == '4x3eports':
        bit_divs     = np.cumsum([0,2*8,24,24,24,24,1])
        bit_discrpts = ['1stZeroPadded_16b', 'ASIC3_24b', 'ASIC2_24b', 'ASIC1_24b', 'ASIC0_24b', 'DataVal_1b']
    elif Scenario == '2x3eports':
        bit_divs     = np.cumsum([0,2*8,24,2*8,24,4*8,1])
        bit_discrpts = ['1stZeroPadded_16b', 'ASIC1_24b', '2ndZeroPadded_16b', 'ASIC0_24b', '3rdZeroPadded_32b', 'DataVal_1b']
    elif Scenario == '2x4eports':
        bit_divs     = np.cumsum([0,2*8,32,8,32,3*8,1])
        bit_discrpts = ['1stZeroPadded_16b', 'ASIC1_32b', '2ndZeroPadded_8b', 'ASIC0_32b', '3rdZeroPadded_24b', 'DataVal_1b']
    elif Scenario == '2x5eports':
        bit_divs     = np.cumsum([0,2*8,40,40,2*8,1])
        bit_discrpts = ['1stZeroPadded_16b', 'ASIC1_40b', 'ASIC0_40b', '2ndZeroPadded_8b', 'DataVal_1b']
    else:
        raise Exception('Scenario: '+ Scenario +' undefined. Please check!')

    #check the defined splits add to 113
    if bit_divs[-1] != 8*14+1: raise Exception('Bit divisions do not add to 113. Please check!')

    #initialise the dictionary with empty strings that will be filled later
    np_arrays  = np.char.array((bit_divs.shape[0]-1)* [''])
    
    return bit_divs, bit_discrpts, np_arrays

def FileToFiberLines(Scenario, Filename):
    #print("NB: The file must NOT start with sync pattern, as we cannot identify it. The sync_patt of 0xcaa (is not unique) corresponds to (StripID:ADC)=(101:10)")
    bit_divs, bit_discrpts, np_bits = PrepareHeader(Scenario)
    #Input line: 4 x 3-elink: (first zero padded bits)8 * 2 + #(second bits)24 * 4 + (last bit)1
    with open(Filename, 'r') as myfile:
        if Filename == 'UTaX4a5.txt':
            pass
            #print('Ignoring the first three lines for', Filename)
            #_ = [myfile.readline() for x in range(3)]

        line = myfile.readline()
        line = line.strip('\n')
        count= 0
        while(line != ''):
            #Actually do not know what the last bit is (?)
            if len(line) != 112+1: raise Exception('The line '+ line + ' is 113 wide. Please check!') 
            #Split the lines according to the defined split taking the scenario into account
            linebits = np.char.array([line[bit_divs[i]: bit_divs[i+1]] for i in range(len(bit_divs)) if i+1 != len(bit_divs)]) 
            np_bits  = np_bits + linebits
            line     = myfile.readline()
            line     = line.strip('\n')
            #if count == 6: break
    
    return np_bits, bit_discrpts

def Identify_Datapacket(chunkbits, asic_readout, bit_divs, pos_toreadfrom, pos_toreadtill, prev_datapackets = None):
    #When the prev_datapackets is None, impose that the first packet for each ASIC MUST be a sync
    sync_patt_fixed = 'fff' #'caa'
    if prev_datapackets is None:
        #the first packet for each ASIC MUST be a sync
        sync_patt = asic_readout[pos_toreadfrom+12:pos_toreadtill+12] #move to next 12b to extract sync_patt
        if conv_hexTobin(sync_patt_fixed, bitwidth = '12b') == sync_patt:  
            #print('The first packet is sync')
            readout   = SyncPacket('Sync', chunkbits, conv_binTohex(sync_patt))
            pos_toreadfrom = pos_toreadtill+12
            return readout, pos_toreadfrom
        else:
            raise Exception('The first packet for each ASIC MUST be a sync. Please check!')
    else:
        #iterate through all previous data packets starting from last checking skipping if it is idle since the bxid of idle is 0000
        prev_bxid = None
        for i in range(len(prev_datapackets)): 
            prev_datap = prev_datapackets[-(i+1)]
            if prev_datap.name != 'Idle': 
                prev_bxid = prev_datap.bxid_full
                break

        #get an estimate of the current bxid
        bxid_estimate = conv_numTobin((conv_binTonum(prev_bxid)+1)%0xDEC, bitwidth = '12b') #estimate what the current bxid should be                               
        #Get the packet header which could be normal/special packet or idle packet or sync packet
        packet_header = chunkbits
        #Incomplete header. Replace with an Idle packet.
        if len(packet_header) < 12:
            print ('Caution! IncompleteHeader ({0}) for the below packet. Some of the header data overflows and is not contained in the input file. Check if your input file has full info!'.format(asic_readout[pos_toreadfrom:]))
            print ('Replaced IncompleteHeader ({0}) with an Idle packet. If you want to convert back to binary format, use RemoveIncompleteData.py first to add Idle packets and synchronize all ASICs.'.format(asic_readout[pos_toreadfrom:]))
            readout = DataPacket('Idle', '0000', '1', '1', '110000')
            pos_toreadfrom = pos_toreadtill
            return readout, pos_toreadfrom
        #first check for sync i.e. packet bxid 12b matches the estimated one AND if the data is sync pattern
        sync_patt = asic_readout[pos_toreadfrom+12:pos_toreadtill+12] #move to next 12b to extract sync_patt
        if bxid_estimate == packet_header and conv_hexTobin(sync_patt_fixed, bitwidth = '12b') == sync_patt:
            #print('This is sync')
            readout = SyncPacket('Sync', packet_header, conv_binTohex(sync_patt))
            pos_toreadfrom = pos_toreadtill+12
            return readout, pos_toreadfrom

        #second check for idle i.e. packet header if 0f0
        idle_header_fixed = '0f0'
        if conv_hexTobin(idle_header_fixed) == packet_header:
            #print('This is Idle')
            readout         = DataPacket('Idle', '0000', '1', '1', '110000')
            pos_toreadfrom  = pos_toreadtill
            return readout, pos_toreadfrom
        
        #third check for normal/special packets
        norm_bxid = packet_header[:4] #get the first 4 bits if it is normal or special bxid
        bxid_estimate_last4b = bxid_estimate[-4:] #extract the last 4b
        if norm_bxid != bxid_estimate_last4b:
            raise Exception('The normal/special bxid does not match the estimate. Please check!')
            print('The normal/special bxid does not match the estimate. Please check!')   
        #split into bxid, parity, flag, length
        splitbits = [chunkbits[bit_divs[i]:bit_divs[i+1]] for i in range(len(bit_divs)) if i+1 != len(bit_divs)]
        if splitbits[2] == '1': #If flag is 1, it is special packet
            packetname = None 
            if splitbits[3] == '010001': 
                packetname = 'BxVeto'
            elif splitbits[3] == '010010':
                packetname = 'HeaderOnly'
            elif splitbits[3] == '010011':
                packetname = 'BusyEvent'
            elif splitbits[3] == '010100':
                packetname = 'BufferFull'
            elif splitbits[3] == '010101':
                packetname = 'BufferFullN'
            elif splitbits[3] == '000110':
                packetname = 'NZS'
            else:
                raise Exception('With the flag of 1, none of the data packets match. Please check!')

            readout = DataPacket(packetname, splitbits[0], splitbits[1], splitbits[2], splitbits[3], bxid_estimate)
            #fill NZS data
            if packetname == 'NZS':
                pos_toreadfrom  = 24 + 128*6 + pos_toreadtill
                if pos_toreadfrom <= len(asic_readout):
                    nzsdata = asic_readout[pos_toreadtill:pos_toreadfrom]
                    readout.data = conv_binTohex(nzsdata)
                else:
                    print('Caution! For the below NZS packet, some of the hit data overflows and is not contained in the input file. Check if your input file has full hit info!')
                    readout.data = 'IncompleteData ({0})'.format(asic_readout[pos_toreadtill:])
            else:
                readout.data   = 'NoData'
                pos_toreadfrom = pos_toreadtill

            return readout, pos_toreadfrom
        elif splitbits[2] == '0': #If flag is zero
            readout         = DataPacket('Normal', splitbits[0], splitbits[1], splitbits[2], splitbits[3], bxid_estimate)
            #get the normal packet hitdata
            nhits           = conv_binTonum(splitbits[-1])
            pos_toreadfrom  = nhits*12 + pos_toreadtill
            if pos_toreadfrom <= len(asic_readout):
                hitdata      = asic_readout[pos_toreadtill:pos_toreadfrom]
                splithitdata = [conv_binTohex(hitdata[i*12:i*12+12]) for i in range(nhits)]
                if len(splithitdata) == 0:
                    readout.data = 'NoHits'
                else:
                    readout.data = [conv_binTohex(hitdata[i*12:i*12+12]) for i in range(nhits)]
            else:
                #readout.data = 'NoData'
                print('Caution! For the below normal packet, some of the hit data overflows and is not contained in the input file. Check if your input file has full hit info!')
                readout.data = 'IncompleteData ({0})'.format(asic_readout[pos_toreadtill:])

            return readout, pos_toreadfrom
        else:
            raise Exception('Flag is not recognished please check!')

def ASICLineSplit(asic_readout):
    #define length of header
    bit_divs     = np.cumsum([0,4,1,1,6]) #bxid, parity, flag, nhits
    if bit_divs[-1] != 12: raise Exception('ASIC header does not add to 12b. Please check!') #check the defined splits add to 113

    pos_toreadfrom  = 0
    readouts        = []
    count           = 0
    while pos_toreadfrom < len(asic_readout):
        linenum        = int(pos_toreadfrom/24)+1
        pos_toreadtill = pos_toreadfrom+12
        chunkbits      = asic_readout[pos_toreadfrom:pos_toreadtill]
        if len(readouts) == 0:
            readout, pos_toreadfrom = Identify_Datapacket(chunkbits, asic_readout, bit_divs, pos_toreadfrom, pos_toreadtill)
        else:
            readout, pos_toreadfrom = Identify_Datapacket(chunkbits, asic_readout, bit_divs, pos_toreadfrom, pos_toreadtill, prev_datapackets = readouts)

        readout.linenum    = int(pos_toreadfrom/24)+1
        readout.zerosafter = 3 #set some dummy variable
        if readout.name != 'Idle' and readout.name != 'Sync':
            print(readout.linenum, readout.name, readout.bxid_full+' ('+str(conv_binTonum(readout.bxid_full))+')', conv_binTonum(readout.parity), conv_binTonum(readout.flag), \
                  readout.length+' ('+str(conv_binTonum(readout.length))+')', readout.data)
        elif readout.name != 'Idle' and readout.name == 'Sync':
            print(readout.linenum, readout.name, readout.bxid_full+' ('+str(conv_binTonum(readout.bxid_full))+')', readout.data)
        elif readout.name == 'Idle':
            print(readout.linenum, readout.name)

        readouts += [readout]
        count += 1
        #if count == 22: break

    return readouts

def write_csv(Newfilename, asicheaders, readouts):
    print('Writing a new file', Newfilename)
    with open(Newfilename, 'w') as csvfile: 
        #print(asicheaders)
        csvfile.write(asicheaders)
        for readout in readouts:
            name = readout.name
            linenum    = readout.linenum
            zerosafter = readout.zerosafter
            if name == 'Idle':
                s = '{0:},{1:},{2:}\n'.format(linenum,zerosafter,name)
            elif name == 'Sync':
                bxidhex   = conv_binTohex(readout.bxid_full)
                bxiddec   = conv_binTonum(readout.bxid_full)
                HitDataHex= readout.data
                s = '{0:},{1:},{2:},{3:},{4:},{5:}\n'.format(linenum,zerosafter,name,bxidhex,bxiddec,HitDataHex)
            else:
                bxidhex   = conv_binTohex(readout.bxid_full)
                bxiddec   = conv_binTonum(readout.bxid_full)
                parity    = conv_binTonum(readout.parity)
                flag      = conv_binTonum(readout.flag)
                lengthHex = conv_binTohex(readout.length)
                lengthdec = conv_binTonum(readout.length)
                HitDataHex= ''
                if not isinstance(readout.data, list):
                    HitDataHex = readout.data
                else:
                    for i in range(len(readout.data)):
                        d       = readout.data[i]
                        databin = conv_hexTobin(d, bitwidth = '12b')
                        sid = conv_binTonum(databin[:7])
                        adc = conv_binTonum(databin[7:12])
                        if i != len(readout.data)-1:
                            HitDataHex  += d+'('+str(sid)+':'+str(adc)+'),'
                        else:
                            HitDataHex  += d+'('+str(sid)+':'+str(adc)+')'

                s = '{0:},{1:},{2:},{3:},{4:},{5:},{6:},{7:},{8:},{9:}\n'.format(linenum,zerosafter,name,bxidhex,bxiddec,parity,flag,lengthHex,lengthdec,HitDataHex)

            #print(s)
            csvfile.write(s)
    
def main():
    #print(newfilename)
    #get the asic data
    np_bits, bit_discrpts = FileToFiberLines(scenario, filename)
    #print(bit_discrpts)

    #write csv files after getting readouts
    asicheaders = '{0:},{1:},{2:},{3:},{4:},{5:},{6:},{7:},{8:},{9:}\n'.format('LineNum','ZerosAfter','PacketName','BXIDHex',\
                  'BXIDDecimal','Parity','Flag','LengthHex','LengthDecimal','HitDataHex(StripID:ADCVal)')
    for i in range(len(bit_discrpts)):
        if 'ASIC' in bit_discrpts[i]:
            asic_name   = bit_discrpts[i].split('_')[0]
            readouts    = ASICLineSplit(np_bits[i])
            write_csv(newfilename.replace('.csv', '_'+asic_name+'_decoded.csv'), asicheaders, readouts)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert UT fiber input to human-redable format. Replace incomplete header with Idle packet.')
    parser.add_argument('-i'  , '--inputfilename' , dest='filename'   , default='UTaX4a5.txt', help='Binary file input (include absolute or relative path) to TELL40 board. Default is UTaX4a5.txt.')
    parser.add_argument('-o'  , '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5.csv', help='Human readable file. Default is Fiber_Decoded_Data/UTaX4a5.csv')
    parser.add_argument('-s'  , '--scenario'      , dest='scenario'   , default='4x3eports'  , help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    args        = parser.parse_args()
    filename    = args.filename
    newfilename = args.newfilename
    scenario    = args.scenario
    print(args)
    main()
