#!/bin/python
import numpy as np
import pandas as pd
import argparse
import warnings
from math import ceil
from util import *
import os
import subprocess
import summarize_CSV

# Input: filename of the event list Questa file (!! NOT TABULAR LIST!!)
#        t0: moment when we start saving data
# Output: a dictionary of dictionaries.
#   example: mysignal['/top/tb/.../clk'][1234000]
#   will give you the value of signal /.../clk at time 1234000
def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split(" ",1)
            mysignal[name]={}
            mysignal[name][0]=value
            #print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split(" ",1)
                    if value_tmp.count("h") == 1:
                        [base, value] = value_tmp.split("h",1)
                    elif value_tmp.count("h") > 1:
                        value_tmp = value_tmp[1:-1]
                        [base, value] = value_tmp.split("h",1)
                        value = value.replace(base+"h","")
                    else:
                        value=value_tmp

                    mysignal[name][moment]=value
            line = myfile.readline().strip('\n')
    return mysignal

# Inputs:
#   thisSignal: a dictionary of {time: value}, for instance mySignal['/.../clk']
#   moment: an integer time, doesn't need to be in the dictionary
# Output:
#   The value of the signal at the specified time, extrapolated from the changes
# Example:
#   findSignalValue(mySignal['/.../clk'], 12345000) will give you the value of clk at that time

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

# Inputs:
#   sampledSignal: what signal to sample, [time, value]
#   clockSignal: what signal triggers the sampling (clk), [time, value]
#   enableSignal: what signal enables the sampling, [time, value]
#           -> 'none' disables this feature and will always sample
#
# Output:
#   a list of (time, value) of the sampled signal
#

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def associateBXID(BXID, Signal):
    BXID_cnt = 0
    associatedSignals = {}
    last_bxid = 0
    for s in Signal:
        timeStamp=s
        values=Signal[s]
        bxid=findSignalValue(BXID, timeStamp)  # hex
        #if bxid == 0xDEB:
        if int(bxid, base=16) < last_bxid:
            BXID_cnt += 1
        bxid_extended = str(BXID_cnt) + bxid
        if bxid_extended in associatedSignals:
            associatedSignals[bxid_extended]+= [values]
        else:
            associatedSignals[bxid_extended]=[values]
        
        last_bxid=int(bxid, base=16)
    return associatedSignals

def join_HL(H, L):
    tmp={}
    for (h, l) in zip(H, L):
        tmp[h]=H[h]+'_'+L[l]
    return tmp

def main():
    #data_ST_source_data_processing_out_name = '/top/tb/TELL40/data_proc/OutputBlock/data_ST_source_data_processing_out'
    #valid_ST_source_data_processing_out_name = '/top/tb/TELL40/data_proc/OutputBlock/valid_ST_source_data_processing_out'
    data_ST_source_data_processing_out_name = '/top/tb/TELL40/data_proc/data_ST_source_data_processing_out'
    valid_ST_source_data_processing_out_name = '/top/tb/TELL40/data_proc/valid_ST_source_data_processing_out'
    BXID_ST_source_data_processing_out_name = '/top/tb/TELL40/data_proc/OutputBlock/BXID_ST_source_data_processing_out'
    FTYPE_ST_source_data_processing_out_name = '/top/tb/TELL40/data_proc/OutputBlock/FTYPE_ST_source_data_processing_out' # FTYPE_8b: x"42" when CH_Flags_needed='0' else x"43"
    TELL40_output_clock_name = '/top/tb/TELL40/data_proc/OutputBlock/TELL40_output_clock'

    mySignals = read_questa(qevtfilename, int(dtime))

    # TFC
    tfc_data_ST_source_data_processing_out = sampleSignal(mySignals['/top/tb/TELL40/data_proc/OutputBlock/tfc_data_ST_source_data_processing_out'],
                                                          mySignals[TELL40_output_clock_name], mySignals['/top/tb/TELL40/data_proc/OutputBlock/tfc_valid_ST_source_data_processing_out'])
    BXID = slice_signal(tfc_data_ST_source_data_processing_out, 64, 64-12, 12) # <= STFC_WORD(63 downto 52)
    first_key = list(BXID.keys())[0]
    first_BXID = BXID[first_key] # This is the first BXID we read from tfc_data_ST_source_data_processing_out
    first_BXID = int(first_BXID,16)
    # Multi Event Packets (MEP)
    mep_accept = slice_signal(tfc_data_ST_source_data_processing_out, 64, 64-14, 1) # <= STFC_WORD(50)
    # TFC trigger
    tfc_trigger = slice_signal(tfc_data_ST_source_data_processing_out, 64, 64-57, 1) # <= STFC_WORD(7)
    cycle = 0
    BXID_prev = '-0xf'
    mep_accept_list = []
    tfc_trigger_list = []
    for key in BXID.keys():
        if (int(BXID[key],16) < int(BXID_prev,16)): cycle = cycle + 1
        if mep_accept[key] in '0123456789ABCDEF':
            aux = "{0:01b}".format(int(mep_accept[key],16))
            mep_accept[key] = aux
            if (aux == '1'): mep_accept_list.append(str(cycle)+":"+str(hex(int(BXID[key],16))))
        if tfc_trigger[key] in '0123456789ABCDEF':
            aux = "{0:01b}".format(int(tfc_trigger[key],16))
            tfc_trigger[key] = aux
            if (aux == '1'): tfc_trigger_list.append(str(cycle)+":"+str(hex(int(BXID[key],16))))
        BXID_prev = BXID[key]

    # SpillOver trigger
    # /top/tb/TELL40/data_proc/SpillOver_0/IB_EventFIFO_Out(83)
    # readout40-firmware/ut/packages/4x3eports/detector_constant_declaration_sim.vhd (CALIBRATION TRIGGERS)
    """
    BXID_SpillOver_IB_name  = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/BXID_SpillOver_IB'
    TRIG_SpillOver_IB_name  = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/TRIG_SpillOver_IB'
    IB_EventFIFO_rdreq_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/IB_EventFIFO_rdreq'
    DP_clock_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/DP_clock'
    BXID_SpillOver_IB = sampleSignal(mySignals[BXID_SpillOver_IB_name], mySignals[DP_clock_name], mySignals[IB_EventFIFO_rdreq_name])
    TRIG_SpillOver_IB = sampleSignal(mySignals[TRIG_SpillOver_IB_name], mySignals[DP_clock_name], mySignals[IB_EventFIFO_rdreq_name])
    cycle = 0
    BXID_prev = '-0xf'
    TRIG_SpillOver_IB_list = []
    for key in BXID_SpillOver_IB.keys():
        if (int(BXID_SpillOver_IB[key],16) < int(BXID_prev,16)): cycle = cycle + 1
        if TRIG_SpillOver_IB[key] in '0123456789ABCDEF':
            aux = "{0:01b}".format(int(TRIG_SpillOver_IB[key],16))
            TRIG_SpillOver_IB[key] = aux
            if (aux == '0'): TRIG_SpillOver_IB_list.append(str(cycle)+":"+str(hex(int(BXID_SpillOver_IB[key],16))))
        BXID_prev = BXID_SpillOver_IB[key]
    """
    
    # DATA
    data_ST_source_data_processing_out = sampleSignal(mySignals[data_ST_source_data_processing_out_name], mySignals[TELL40_output_clock_name], 'none')
    valid_ST_source_data_processing_out = sampleSignal(mySignals[valid_ST_source_data_processing_out_name], mySignals[TELL40_output_clock_name], 'none')
    BXID_ST_source_data_processing_out = sampleSignal(mySignals[BXID_ST_source_data_processing_out_name], mySignals[TELL40_output_clock_name], 'none')
    FTYPE_ST_source_data_processing_out = sampleSignal(mySignals[FTYPE_ST_source_data_processing_out_name], mySignals[TELL40_output_clock_name], 'none')

    for key in valid_ST_source_data_processing_out.keys(): # Conversion from hex to binary 
        if valid_ST_source_data_processing_out[key] in '0123456789ABCDEF':
            aux = "{0:01b}".format(int(valid_ST_source_data_processing_out[key],16))
            valid_ST_source_data_processing_out[key] = aux

    ofile = open(newfilename+'.txt', 'w')
    cont = 0
    for key in data_ST_source_data_processing_out.keys():
        cont = cont + 1
        if cont != 1: # format cleaning
            data = data_ST_source_data_processing_out[key]
            if (str(valid_ST_source_data_processing_out[key]) == '1'): # write only valid
                outstring = str(bin(int(data,16))[2:].zfill(256))+str(valid_ST_source_data_processing_out[key])+ \
                            str(BXID_ST_source_data_processing_out[key])+str(FTYPE_ST_source_data_processing_out[key])+'\n'
                ofile.write(outstring)
    ofile.close()

    # Active ASICs
    # readout40-firmware/tell40/top_level_TELL40.vhd
    # tell40_aligned_fiber_reg <= x"00efffff";
    # limit_accepted_events <= x"10000100";
    tell40_aligned_fiber_name = "/top/tb/TELL40/data_proc/tell40_aligned_fiber"; status_aligned = 0; c_no = 0
    # Shift ASICs
    # readout40-firmware/ut/data_processing/Packages/UTCommonPackage.vhd
    # LaneXShiftAsics (X = 0,...,5)
    Lane_Shift_ASICS_name = "/top/tb/TELL40/data_proc/InputBlock_"+lane+"/Lane_Shift_ASICS"; status_shift = 0
    with open(qevtfilename, 'r') as fp:
        for l_no, line in enumerate(fp):
            if tell40_aligned_fiber_name in line: c_no = c_no + 1
            if tell40_aligned_fiber_name in line and c_no == 2:
                tell40_aligned_fiber = line.strip()[-6:]
                if (nASICs == 2):
                    #tell40_aligned_fiber = "{0:12b}".format(int(tell40_aligned_fiber[:3],16)) # No leading zeros
                    tell40_aligned_fiber = bin(int(tell40_aligned_fiber[:3],16))[2:].zfill(12)
                    tell40_aligned_fiber_list = [tell40_aligned_fiber[start:start+2] for start in range(0, len(tell40_aligned_fiber), 2)]
                    tell40_aligned_asics = list(tell40_aligned_fiber_list[int(lane)])
                if (nASICs == 4): 
                    #tell40_aligned_asics = list("{0:04b}".format(int(tell40_aligned_fiber[int(lane)],16))) # No leading zeros
                    tell40_aligned_asics = list(bin(int(tell40_aligned_fiber[int(lane)],16))[2:].zfill(4))
                tell40_aligned_asics = list(map(int, tell40_aligned_asics))
                tell40_aligned_asics.reverse()
                status_aligned = 1
            if Lane_Shift_ASICS_name in line:
                s = line.replace("2'h", "") 
                bracket = s[s.find("{")+1:s.find("}")]
                Lane_Shift_ASICS = list(map(int,bracket.split())) 
                if (nASICs == 2): del Lane_Shift_ASICS[:2]
                Lane_Shift_ASICS.reverse()
                status_shift = 1
            if (status_aligned == 1 and status_shift == 1): break
    if (status_aligned == 0 and nASICs == 2): print ("tell40_aligned_fiber signal not found. Activating all ASICs..."); tell40_aligned_asics = [1, 1]
    if (status_aligned == 0 and nASICs == 4): print ("tell40_aligned_fiber signal not found. Activating all ASICs..."); tell40_aligned_asics = [1, 1, 1, 1]
    if (status_shift == 0 and nASICs == 2): print ("LaneXShiftAsics signal not found. Deactivating shift..."); Lane_Shift_ASICS = [0, 1]
    if (status_shift == 0 and nASICs == 4): print ("LaneXShiftAsics signal not found. Deactivating shift..."); Lane_Shift_ASICS = [0, 1, 2, 3]

    # Change endianness
    Output_width = 32*[8]; Output_width.append(1); Output_width.append(3); Output_width.append(2)
    df = pd.read_fwf(newfilename+'.txt', widths=Output_width, header=None, dtype=str)
    cols = df.columns.tolist()
    cols = [val for i in range(0, len(cols)-3, 4) for val in reversed(cols[i:i+4])]
    cols.append(32); cols.append(33); cols.append(34);
    df = df[cols] # Change endianness
    np.savetxt(newfilename+'_Endianness.txt', df.values, fmt = "%s", delimiter="")

    # Compare with TELL40 input
    print ("Processing the events...")
    Output_names = ['Header','Lane5','Lane4','Lane3','Lane2','Lane1','Lane0','valid','BXID','FTYPE']
    Output_width = [64,32,32,32,32,32,32,1,3,2]
    df = pd.read_fwf(newfilename+'_Endianness.txt', widths=Output_width, names=Output_names, header=None, dtype=str)
    df['BXID_next'] = df['BXID'].shift(-1)
    df['BXID_prev'] = df['BXID'].shift(1)
    rows_count = len(df.index)

    ofile = open(newfilename+'_decoded_summary.csv', 'w')
    cycle = 0
    event_hits = ''
    flag_header = ''
    ofile.write('BXIDHex,Packets,Lengths,TLen,Hits,Cycle\n')
    for index, row in df.iterrows():
        if (index != 0 and int(row['BXID'],16) < int(row['BXID_prev'],16)): cycle = cycle + 1
        string_extra_info = "{:02d}".format(cycle)
        event_hits = row['Lane{}'.format(lane)] + event_hits
        if (row['BXID'] != row['BXID_prev']):
            event_header = row['Header']
        if (row['BXID'] == row['BXID_prev']):
            flag_header = row['Header'] + flag_header
        if (row['BXID'] != row['BXID_next']):
            eventID = int(event_header[:12],2)
            reserved_bits = event_header[12:16]
            event_header = event_header[-8*6:]
            event_header_list = [event_header[start:start+8] for start in range(0, len(event_header), 8)]
            event_header_list.reverse()
            asic_header_list = ['U','U','U','U']
            asic_length_list = [0,0,0,0]
            asic_hitinfo_list = ['0','0','0','0']
            # All active ASICs send same special data type
            # HeaderOnly, BxVeto and Sync
            if (row['FTYPE'] == '63'):
                # First bit: All ASICs are disabled or not
                # Second bit: All active ASICs are sending the same data type or not
                # Next two bits: 00 if all active ASICs send Sync
                #                01 for HeaderOnly
                #                10 for BxVeto
                #                11 for other cases
                if (reserved_bits == '0101'): 
                    asic_header_list = ['HO','HO','HO','HO']
                    for i in range (nASICs): asic_length_list[i] = 18
                if (reserved_bits == '0110'): 
                    asic_header_list = ['BxVeto','BxVeto','BxVeto','BxVeto']
                    for i in range (nASICs): asic_length_list[i] = 17
            if (row['FTYPE'] == '44'):
                flag_header = flag_header[-192:]
                flag_header_list = [flag_header[start:start+32] for start in range(0, len(flag_header), 32)]
                flag_header_list.reverse()
                asic_header = flag_header_list[int(lane)]
                asic_header_list = [asic_header[start:start+8] for start in range(0, len(asic_header), 8)]
                asic_header_list.reverse()
                for i in range (nASICs):
                    if (asic_header_list[i][-7:] == '1010010'): asic_header_list[i] = 'HO'; asic_length_list[i] = 18
                    if (asic_header_list[i][-7:] == '1000110'): asic_header_list[i] = 'NZS'; asic_length_list[i] = 6
                    if (asic_header_list[i][-7:] == '1010001'): asic_header_list[i] = 'BxVeto'; asic_length_list[i] = 17
                    if (asic_header_list[i][-7:] == '1010011'): asic_header_list[i] = 'BusyEvent'; asic_length_list[i] = 19
                    if (asic_header_list[i][-7:] == '1010100'): asic_header_list[i] = 'BufferFull'; asic_length_list[i] = 20
                    if (asic_header_list[i][-7:] == '1010101'): asic_header_list[i] = 'BufferFullN'; asic_length_list[i] = 21
                    if (asic_header_list[i][1]   == '0'      ): asic_header_list[i] = 'NP'
                if ([asic_header_list[0]]*len(asic_header_list) == asic_header_list and asic_header_list[0] == "NZS"):
                    print ("BXID ", hex(int(row['BXID'],16)), ": Error - FTYPE = 44 and all active ASICs are sending NZS.", sep='')
            if (row['FTYPE'] == '42'): asic_header_list = ['NP','NP','NP','NP']
            if (row['FTYPE'] == '62'):
                asic_header_list = ['NZS','NZS','NZS','NZS']
                for i in range (nASICs): asic_length_list[i] = 6
            asic_header_list_active = []; 
            for i in range(len(tell40_aligned_asics)):
                j = Lane_Shift_ASICS[i]
                if (tell40_aligned_asics[i] == 1): 
                    asic_header_list_active.append(asic_header_list[j])
            if (row['FTYPE'] == '42' or (row['FTYPE'] == '44' and 'NP' in asic_header_list_active and 'NZS' not in asic_header_list_active)):
                asic_hits_list = [event_hits[start:start+16] for start in range(0, len(event_hits), 16)]
                asic_hits_list.reverse()
                #asic_hits_list = [i for i in asic_hits_list if int(i,2) != 0] # With SpillOver ADC value can be zero
                asic_hits_list = asic_hits_list[:int(event_header_list[int(lane)],2)]
                for i in asic_hits_list:
                    asic_id = int(i[:4],2)
                    if (asic_id > nASICs-1): print ("BXID ", hex(int(row['BXID'],16)), ": Error - NP with hit ASIC ID = ", asic_id, ". Hit excluded.", sep=''); continue
                    asic_length_list[asic_id] = asic_length_list[asic_id] + 1
                    asic_hitinfo_list[asic_id] = asic_hitinfo_list[asic_id] + str(i[-12:])
            if (row['FTYPE'] == '62' or (row['FTYPE'] == '44' and 'NZS' in asic_header_list_active and 'NP' not in asic_header_list_active)):
                asic_hits_list = [event_hits[start:start+8+24+128*8] for start in range(0, len(event_hits), 8+24+128*8)]
                zpad = 0 # Remove zero padded entries
                for i in range(len(asic_hits_list)):
                    if (asic_hits_list[i] == "0"*(8+24+128*8)): zpad = 1
                if (zpad == 1): asic_hits_list.remove("0"*(8+24+128*8))
                nHitsLane = int(event_header_list[int(lane)],2)
                for i in range(0,len(asic_hits_list)):
                    nzsHead = asic_hits_list[i][-24:]
                    asic_id = int(asic_hits_list[i][-32:-24],2)
                    ch_hits = asic_hits_list[i][:(128*8)]
                    ch_hits_list = [ch_hits[start:start+8] for start in range(0, len(ch_hits), 8)]
                    for j in range(0,len(ch_hits_list)): ch_hits_list[j] = ch_hits_list[j][2:]
                    ch_hits_list.reverse()
                    asic_hitinfo_list[asic_id] = nzsHead + ''.join(ch_hits_list)
            TLen = sum(asic_length_list)
            string_bxid = hex(int(row['BXID'],16))
            string_asic_header = '/'.join(asic_header_list[:nASICs][::-1])
            string_asic_length = '/'.join(["{:02d}".format(item) for item in asic_length_list[:nASICs][::-1]])
            string_tlen = "{:03d}".format(TLen)
            string_asic_hitinfo = '/'.join(["{}".format(str(conv_binTohex(item))) for item in asic_hitinfo_list[:nASICs][::-1]])
            print ("BXID {}: EventID {}".format(string_bxid,eventID))
            ofile.write(string_bxid+','+string_asic_header+','+string_asic_length+','+string_tlen+','+string_asic_hitinfo+','+string_extra_info+'\n')
            event_hits = ''
            flag_header = ''
    ofile.close()

    df_input  = pd.read_csv(filename)
    df_output = pd.read_csv(newfilename+'_decoded_summary.csv')
    df_input  = df_input.iloc[:,:-1] # Remove last column
    # Add cycle column
    cycle_list = []
    cycle = 0
    for index, row in df_input.iterrows():
        if (row['BXIDHex'] == '0x0'): cycle = cycle + 1
        cycle_list.append(cycle)      
    df_input["Cycle"] = cycle_list  

    bxid_ini = df_output['BXIDHex'].iloc[0]
    bxid_fin = df_output['BXIDHex'].iloc[-1]
    cycl_ini = df_output['Cycle'].iloc[0]
    cycl_fin = df_output['Cycle'].iloc[-1]
    index_first = df_input.loc[ (df_input['BXIDHex'] == bxid_ini) & (df_input['Cycle'] == cycl_ini) ].index[0]
    df_input.drop(df_input.index[0:index_first], inplace=True)
    df_input.reset_index(drop=True, inplace = True)
    index_last = df_input.loc[ (df_input['BXIDHex'] == bxid_fin) & (df_input['Cycle'] == cycl_fin) ].index[0]
    df_input.drop(df_input.index[index_last+1:], inplace=True)

    # Merge two dataFrames and add indicator column
    df_all = pd.merge(df_input, df_output, on=['BXIDHex','Packets','Lengths','TLen','Hits','Cycle'], how='left', indicator='Exists')
    # Add column to show if each row in first DataFrame exists in second
    df_all['Exists'] = np.where(df_all.Exists == 'both', True, False)

    OutputBlock_summary_list = []
    for index, row in df_all.iterrows():
        OutputBlock_summary_temp = df_output[ (df_output['BXIDHex'] == row['BXIDHex']) & (df_output['Cycle'] == row['Cycle'])].to_string(index=False, header=None)
        if (OutputBlock_summary_temp[:15] == "Empty DataFrame"):
            # Choose tfc_trigger_list or TRIG_SpillOver_IB_list
            if (str(row['Cycle'])+":"+str(row['BXIDHex']) not in tfc_trigger_list):   OutputBlock_summary_temp = "SO Trigger"
            #if (str(row['Cycle'])+":"+str(row['BXIDHex']) in TRIG_SpillOver_IB_list): OutputBlock_summary_temp = "SO Trigger"
            else:                                                                     OutputBlock_summary_temp = "Empty BXID"
        OutputBlock_summary_list.append(OutputBlock_summary_temp)
    df_all["OutputBlock"] = OutputBlock_summary_list

    print ("TELL40 Input/OutputBlock comparison:")
    print (df_all.to_string(max_colwidth=80))
    print ("TELL40 Input/OutputBlock differences:")
    print (df_all[df_all['Exists'] == False].to_string(max_colwidth=80))
    print ("TELL40 Input/OutputBlock ASIC differences:")
    if (nASICs == 2): print ("Active  ASICs [ASIC0, ASIC1]:", tell40_aligned_asics)
    if (nASICs == 4): print ("Active  ASICs [ASIC0, ASIC1, ASIC2, ASIC3]:", tell40_aligned_asics)
    if (nASICs == 2): print ("Shifted ASICs [ASIC0, ASIC1]:", Lane_Shift_ASICS)
    if (nASICs == 4): print ("Shifted ASICs [ASIC0, ASIC1, ASIC2, ASIC3]:", Lane_Shift_ASICS)
    status_asics = 1
    for i in range (nASICs):
        Cycle_list = []
        BXIDHex_list = []
        index_list = []
        Packet_input_list = []
        Length_input_list = []
        Hits_input_list = []
        Packet_output_list = []
        Length_output_list = []
        Hits_output_list = []
        if (tell40_aligned_asics[i] == 1): print ("ASIC", i, " is active. Proceed...", sep='')
        if (tell40_aligned_asics[i] == 0): print ("ASIC", i, " is inactive. Skip...", sep=''); continue
        j = Lane_Shift_ASICS[i] 
        if (i != j): print ("ASIC", i, " is shifted to ASIC", j, ". Proceed...", sep='')
        for index, row in df_all.iterrows():
            if (row['OutputBlock'] == "SO Trigger"): continue
            Cycle_list.append(row['Cycle'])
            BXIDHex_list.append(row['BXIDHex'])
            index_list.append(str(row['Cycle'])+":"+str(row['BXIDHex']))
            # TELL40 Input
            temp = row['Packets'].split('/'); temp.reverse(); Packet_input_list.append(temp[i])
            temp = row['Lengths'].split('/'); temp.reverse(); Length_input_list.append(temp[i])
            temp = row['Hits'].split('/');    temp.reverse(); Hits_input_list.append(temp[i])
            # TELL40 Output
            if (row['OutputBlock'] == "SO Trigger"):
                Packet_output_list.append("SO Trigger")
                Length_output_list.append("SO Trigger")
                Hits_output_list.append("SO Trigger")
            elif (row['OutputBlock'] == "Empty BXID"):
                Packet_output_list.append("Empty BXID")
                Length_output_list.append("Empty BXID")
                Hits_output_list.append("Empty BXID")
            else:
                temp = row['OutputBlock'].split()[1]; temp = temp.split('/'); temp.reverse(); Packet_output_list.append(temp[j]) 
                temp = row['OutputBlock'].split()[2]; temp = temp.split('/'); temp.reverse(); Length_output_list.append(temp[j])
                temp = row['OutputBlock'].split()[4]; temp = temp.split('/'); temp.reverse(); Hits_output_list.append(temp[j])
        df_input_asics = pd.DataFrame(list(zip(Cycle_list, BXIDHex_list, Packet_input_list, Length_input_list, Hits_input_list)), index = index_list, columns = ['Cycle', 'BXIDHex', 'Packet', 'Length', 'Hits'])
        df_output_asics = pd.DataFrame(list(zip(Cycle_list, BXIDHex_list, Packet_output_list, Length_output_list, Hits_output_list)), index = index_list, columns = ['Cycle', 'BXIDHex', 'Packet', 'Length', 'Hits'])
        print (df_input_asics.compare(df_output_asics, result_names=("Input","Output")).to_string())
        if (not df_input_asics.compare(df_output_asics).empty): status_asics = 0

    numeric = '0123456789-.'
    for i,c in enumerate(runtime):
        if c not in numeric:
            break
    t_numb = float(runtime[:i])
    t_unit = runtime[i:].lstrip()
    if (t_unit=='fs') : conv_const = 1e-15
    if (t_unit=='ps') : conv_const = 1e-12
    if (t_unit=='ns') : conv_const = 1e-9
    if (t_unit=='us') : conv_const = 1e-6
    if (t_unit=='ms') : conv_const = 1e-3
    if (t_unit=='sec'): conv_const = 1.0
    if (t_numb != 0): bandwidth_run = (rows_count*256)/(conv_const*t_numb)
    else:             bandwidth_run = 0.0
    gigabit = 1e9
    print ("Number of bits / runtime:", round(bandwidth_run/gigabit,2), "Gbits/s")
    bxid_count = len(df_input.index)
    bandwidth_pcie = (rows_count*256)/(bxid_count*25.0e-9)
    # tail -n 3 sim-pcie40v2-ut-*/read/ReadOutputBlock_LN_*.log
    print ("PCIe data bandwidth:", round(bandwidth_pcie/gigabit,2), "Gbits/s")
    rej_bxids = len(df_all[df_all['OutputBlock'] == 'SO Trigger'].index)
    all_bxids = len(df_all.index)
    acc_bxids = all_bxids - rej_bxids
    trg_frequ = 40.0*acc_bxids/all_bxids
    print ("TFC Trigger frequency:", round(trg_frequ), "MHz")
    if (status_asics == 0): print ("Status: *Error*")
    if (status_asics == 1): print ("Status: All OK!")
    print('Done!')
   
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Summary of humand-redable UT TELL40 OutputBlock. The numbering of ASICs in the summary file goes from right to left e.g. ASIC3/ASIC2/ASIC1/ASIC0.')
    parser.add_argument('-i', '--inputfilename', dest='filename', default='Fiber_Decoded_Data/UTaX4a5_TELL40Input_decoded_summary.csv',
                        help='Summary of human readable TELL40 input file for ASICs. Default is Fiber_Decoded_Data/UTaX4a5_TELL40Input_decoded_summary.csv')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5',
                        help='Output files. Default is Fiber_Decoded_Data/UTaX4a5. Do not use a file extension. A binar file will be created as <newfilename>.txt. A summary file will be created as <newfilename>_decoded_summary.csv')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports (obsolete), 2x4eports, 2x5eports. The 2x3eports case has 4 ASICs per Lane: Use 4x3eports option.')
    parser.add_argument('-q', '--questafilename', dest='qevtfilename', default='Questa_Data/list_UTaX4a5.lst',
                        help='Filename of the event list Questa file. Default is Questa_Data/list_UTaX4a5.lst')
    parser.add_argument('-l', '--lane', dest='lane', default='0',
                        help='Lane of interest for the event list Questa file. Default is 0')
    parser.add_argument('-r', '--runtime', dest='runtime', default='250us',
                        help='Specify the runtime with units. Default is 250us')
    parser.add_argument('-t', '--dtime', dest='dtime', default='119380000',
                        help='Specify the time we start saving data in ps. Default is 119380000')
    args         = parser.parse_args()
    filename     = args.filename
    newfilename  = args.newfilename
    scenario     = args.scenario
    qevtfilename = args.qevtfilename 
    lane         = args.lane
    runtime      = args.runtime
    dtime        = args.dtime
    print(args)
    if (scenario == '4x3eports'): nASICs = 4; bitwidth = 24
    if (scenario == '2x3eports'): nASICs = 2; bitwidth = 24
    if (scenario == '2x4eports'): nASICs = 2; bitwidth = 32
    if (scenario == '2x5eports'): nASICs = 2; bitwidth = 40
    print ("Processing Questa file...")
    main()
