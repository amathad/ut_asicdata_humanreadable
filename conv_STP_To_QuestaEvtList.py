#!/bin/python
import pandas as pd
import numpy as np
import argparse

def main():
    # Search string 'time unit:' in input file
    with open(filename, 'r') as fp:
        for l_no, line in enumerate(fp):
            # Search string
            if 'time unit:' in line:
                tunit = line[11:13]
                # Don't look for next lines
                break

    # Read STP data
    df = pd.read_csv(filename, skiprows=l_no, low_memory=False)
    # Dictionary of filtered signals. Each hex digit handles four bits
    signal_filter = {"time unit: " + tunit: "@"}

    # -/InputBlock_0/EventFIFO {/top/tb/TELL40/data_proc/InputBlock_0/EventFIFO/q(162 downto 0)
    # -/LaneBuilder_0 {/top/tb/TELL40/data_proc/LaneBuilder_0/TempFIFO_Out(134 downto 0)
    # -/SpillOver_0 {/top/tb/TELL40/data_proc/SpillOver_0/SO_EventFIFO_Out(162 downto 0)
    # -/top/tb/TELL40/data_proc/SpillOver_0 {/top/tb/TELL40/data_proc/SpillOver_0/IB_EventFIFO_Out(162 downto 0)

    # Loop over lanes
    for i in lanes:
        # InputBlock
        if (block == "InputBlock"):
            path_signal_tap = " top_level_TELL40:TELL40_0|data_processing:data_proc|DP_InputBlock:InputBlock_{}|".format(i)
            path_simulation = "/top/tb/TELL40/data_proc/InputBlock_{}/".format(i)
            list_combine = []
            for j in range (nASICs-1,-1,-1): list_combine.append(path_signal_tap+"data_ST_sink_data_processing["+str(j)+"][0.."+str(bitwidth-1)+"]")
            df[path_signal_tap+"data_ST_sink_data_processing"] = df[list_combine].apply(''.join, axis=1).str.replace('\s+','',regex=True)
            signal_filter[path_signal_tap+"data_ST_sink_data_processing"] = path_simulation+"data_ST_sink_data_processing "+sink
            signal_filter[path_signal_tap+"valid_ST_sink_data_processing["+str(nASICs-1)+"..0]"] = path_simulation+"valid_ST_sink_data_processing "+str(nASICs)+"'hX"
            signal_filter[" top_level_TELL40:TELL40_0|data_processing:data_proc|data_ST_sink_tfc_processing[63..0]"] = path_simulation+"data_ST_sink_tfc_processing 64'hXXXXXXXXXXXXXXXX"
            signal_filter[" top_level_TELL40:TELL40_0|data_processing:data_proc|valid_ST_sink_tfc_processing"] = path_simulation+"valid_ST_sink_tfc_processing X"
            list_combine = []
            for j in range(76,69-1,-1): list_combine.append(path_signal_tap+"dcfifo:EventFIFO|data["+str(j)+"]")
            df[path_signal_tap+"FTYPE_In"] = df[list_combine].apply(''.join, axis=1).str.replace('\s+','',regex=True)
            df[path_signal_tap+"FTYPE_In"] = df[path_signal_tap+"FTYPE_In"].astype(str).apply(lambda s: hex(int(s,2))[2:].upper().zfill(2) if "X" not in s else "XX")
            signal_filter[path_signal_tap+"FTYPE_In"] = path_simulation+"FTYPE_In 8'hXX"
            list_combine = []
            for j in range (nASICs-1,-1,-1): list_combine.append(path_signal_tap+"DP_AsicShifter:AsicShifter|Shift_ASICS["+str(j)+"][0..1]")
            df[path_signal_tap+"Lane_Shift_ASICS"] = df[list_combine].apply(''.join, axis=1).str.replace('\s+','',regex=True)
            sv = df[path_signal_tap+"Lane_Shift_ASICS"].iat[1]
            df[path_signal_tap+"Lane_Shift_ASICS"].iat[0] = sv
            signal_filter[path_signal_tap+"Lane_Shift_ASICS"] = path_simulation+"Lane_Shift_ASICS {2'h"+sv[0]+" 2'h"+sv[1]+" 2'h"+sv[2]+" 2'h"+sv[3]+"}"
        # SpillOver
        if (block == "SpillOver"):
            path_signal_tap = " top_level_TELL40:TELL40_0|data_processing:data_proc|DP_SpillOver:SpillOver_{}|".format(i)
            path_second_stp = " top_level_TELL40:TELL40_0|data_processing:data_proc|DP_InputBlock:InputBlock_{}|dcfifo:".format(i)
            path_simulation = "/top/tb/TELL40/data_proc/SpillOver_{}/".format(i)
            list_combine = []
            for j in range (3,-1,-1): list_combine.append(path_signal_tap+"SO_DataOut["+str(j)+"][0..31]")
            df[path_signal_tap+"SO_DataOut"] = df[list_combine].apply(''.join, axis=1).str.replace('\s+','',regex=True)
            signal_filter[path_signal_tap+"SO_DataOut"] = path_simulation+"SO_DataOut {32'hXXXXXXXX 32'hXXXXXXXX 32'hXXXXXXXX 32'hXXXXXXXX}"
            df[path_signal_tap+"BXID_SpillOver_SO"] = df[path_signal_tap+"SO_EventFIFO_Out[159..0]"].astype(str).apply(lambda s: hex(int(bin(int(s,16))[2:].zfill(160)[-139-1:-128],2))[2:].upper().zfill(3) if "X" not in s else "XXX")
            signal_filter[path_signal_tap+"BXID_SpillOver_SO"] = path_simulation+"BXID_SpillOver_SO 12'hXXX"
            df[path_signal_tap+"HEAD_SpillOver_SO"] = df[path_signal_tap+"SO_EventFIFO_Out[159..0]"].astype(str).apply(lambda s: hex(int(bin(int(s,16))[2:].zfill(160)[-31-1:],2))[2:].upper().zfill(8) if "X" not in s else "XXXXXXXX")
            signal_filter[path_signal_tap+"HEAD_SpillOver_SO"] = path_simulation+"HEAD_SpillOver_SO 32'hXXXXXXXX"
            df[path_signal_tap+"TRIG_SpillOver_IB"] = df[path_second_stp+"EventFIFO|q[155..0]"].astype(str).apply(lambda s: hex(int(bin(int(s,16))[2:].zfill(156)[-83-1],2))[2:].upper().zfill(1) if "X" not in s else "X")
            signal_filter[path_signal_tap+"TRIG_SpillOver_IB"] = path_simulation+"TRIG_SpillOver_IB X"
            df[path_signal_tap+"BXID_SpillOver_IB"] = df[path_second_stp+"EventFIFO|q[155..0]"].astype(str).apply(lambda s: hex(int(bin(int(s,16))[2:].zfill(156)[-139-1:-128],2))[2:].upper().zfill(3) if "X" not in s else "XXX")
            signal_filter[path_signal_tap+"BXID_SpillOver_IB"] = path_simulation+"BXID_SpillOver_IB 12'hXXX"            
            signal_filter[path_second_stp+"EventFIFO|rdreq"] = path_simulation+"IB_EventFIFO_rdreq X"
            signal_filter[path_signal_tap+"SO_DataInRdReq[3..0]"] = path_simulation+"SO_DataInRdReq 4'hX"
            sv = df[path_signal_tap+"DP_SpillOver_Correction:DP_SO_Corr|DP_SpillOver_fill_registers:\SOCorrectionGen:0:DP_SO_fill_registers|bypass_SO"].iat[1]
            df[path_signal_tap+"DP_SpillOver_Correction:DP_SO_Corr|DP_SpillOver_fill_registers:\SOCorrectionGen:0:DP_SO_fill_registers|bypass_SO"].iat[0] = sv
            signal_filter[path_signal_tap+"DP_SpillOver_Correction:DP_SO_Corr|DP_SpillOver_fill_registers:\SOCorrectionGen:0:DP_SO_fill_registers|bypass_SO"] = path_simulation+"bypass_SO "+sv.strip()
            df[path_signal_tap+"BXID_Layer1"] = df[path_second_stp+"EventFIFO|q[155..0]"].astype(str).apply(lambda s: hex(int(bin(int(s,16))[2:].zfill(156)[-139-1:-128],2))[2:].upper().zfill(3) if "X" not in s else "XXX") # LaneBuilder
            signal_filter[path_signal_tap+"BXID_Layer1"] = "/top/tb/TELL40/data_proc/InputBlock_{}/EventFIFO/BXID_Layer1 12'hXXX".format(i) # LaneBuilder
        # LaneBuilder
        if (block == "LaneBuilder"):
            path_signal_tap = " top_level_TELL40:TELL40_0|data_processing:data_proc|DP_LaneBuilder:LaneBuilder_{}|".format(i)
            path_simulation = "/top/tb/TELL40/data_proc/LaneBuilder_{}/".format(i)
            df[path_signal_tap+"BXID_Layer2"] = df[path_signal_tap+"scfifo:TempFIFO|q[131..0]"].astype(str).apply(lambda s: hex(int(bin(int(s,16))[2:].zfill(132)[-127-1:-116],2))[2:].upper().zfill(3) if "X" not in s else "XXX")
            signal_filter[path_signal_tap+"BXID_Layer2"] = path_simulation+"BXID_Layer2 12'hXXX"
            # MiniLaneBuilder 01
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_01|scfifo:EventFIFO_H|data[15..0]"] = path_simulation+"DP_MiniLB_01/EventFIFO_H_In 16'hXXXX"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_01|scfifo:EventFIFO_L|data[15..0]"] = path_simulation+"DP_MiniLB_01/EventFIFO_L_In 16'hXXXX"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_01|scfifo:EventFIFO_H|wrreq"] = path_simulation+"DP_MiniLB_01/EventFIFO_H_wrreq X"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_01|scfifo:EventFIFO_L|wrreq"] = path_simulation+"DP_MiniLB_01/EventFIFO_L_wrreq X"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_01|StartProcessing"] = path_simulation+"StartProcessing_01 X"
            # MiniLaneBuilder 23
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:\DP_MiniLB_23_flavour:DP_MiniLB_23|scfifo:EventFIFO_H|data[15..0]"] = path_simulation+"DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_H_In 16'hXXXX"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:\DP_MiniLB_23_flavour:DP_MiniLB_23|scfifo:EventFIFO_L|data[15..0]"] = path_simulation+"DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_L_In 16'hXXXX"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:\DP_MiniLB_23_flavour:DP_MiniLB_23|scfifo:EventFIFO_H|wrreq"] = path_simulation+"DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_H_wrreq X"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:\DP_MiniLB_23_flavour:DP_MiniLB_23|scfifo:EventFIFO_L|wrreq"] = path_simulation+"DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_L_wrreq X"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:\DP_MiniLB_23_flavour:DP_MiniLB_23|StartProcessing"] = path_simulation+"StartProcessing_23 X"
            # MiniLaneBuilder 0123
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_0123|scfifo:EventFIFO_H|data[15..0]"] = path_simulation+"DP_MiniLB_0123/EventFIFO_H_In 16'hXXXX"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_0123|scfifo:EventFIFO_L|data[15..0]"] = path_simulation+"DP_MiniLB_0123/EventFIFO_L_In 16'hXXXX"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_0123|scfifo:EventFIFO_H|wrreq"] = path_simulation+"DP_MiniLB_0123/EventFIFO_H_wrreq X"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_0123|scfifo:EventFIFO_L|wrreq"] = path_simulation+"DP_MiniLB_0123/EventFIFO_L_wrreq X"
            signal_filter[path_signal_tap+"DP_MiniLaneBuilder:DP_MiniLB_0123|StartProcessing"] = path_simulation+"StartProcessing_0123 X"
    # OutputBlock
    if (block == "OutputBlock"):
        path_signal_tap = " top_level_TELL40:TELL40_0|data_processing:data_proc|UT_DP_OutputBlock:OutputBlock|"
        path_simulation = "/top/tb/TELL40/data_proc/OutputBlock/"
        signal_filter[path_signal_tap+"BXID_ST_source_data_processing_out[11..0]"] = path_simulation+"BXID_ST_source_data_processing_out 12'hXXX"
        signal_filter[path_signal_tap+"FTYPE_ST_source_data_processing_out[7..0]"] = path_simulation+"FTYPE_ST_source_data_processing_out 8'hXX"
        signal_filter[path_signal_tap+"data_ST_source_data_processing_out[255..0]"] = path_simulation+"data_ST_source_data_processing_out 256'hXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        signal_filter[path_signal_tap+"valid_ST_source_data_processing_out"] = path_simulation+"valid_ST_source_data_processing_out X"
        # Signals with the new endianness 
        signal_filter[" top_level_TELL40:TELL40_0|data_processing:data_proc|data_ST_source_data_processing_out[255..0]"] = "/top/tb/TELL40/data_proc/data_ST_source_data_processing_out 256'hXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
        signal_filter[path_signal_tap+"valid_ST_source_data_processing_out"] = "/top/tb/TELL40/data_proc/valid_ST_source_data_processing_out X"
        #################################
        signal_filter[path_signal_tap+"tfc_data_ST_source_data_processing_out[63..0]"] = path_simulation+"tfc_data_ST_source_data_processing_out 64'hXXXXXXXXXXXXXXXX"
        signal_filter[path_signal_tap+"tfc_valid_ST_source_data_processing_out"] = path_simulation+"tfc_valid_ST_source_data_processing_out X"
        signal_filter[" top_level_TELL40:TELL40_0|data_processing:data_proc|tell40_aligned_fiber[23..0]"] = "/top/tb/TELL40/data_proc/tell40_aligned_fiber 24'hXXXXXX"

    # Lists of keys and values of filtered signals
    keysList   = [i for i in signal_filter.keys()]
    valuesList = [i for i in signal_filter.values()]

    # Filter signals
    df = df[keysList]
    df.rename(columns=dict(zip(keysList, valuesList)), inplace=True)
    dt = (df.iloc[1]['@'] - df.iloc[0]['@'])/2.0 # Time step

    #define new DataFrame as original DataFrame with each row repeated 3 times
    df_new = pd.DataFrame(np.repeat(df.values, 2, axis=0))
    #assign column names of original DataFrame to new DataFrame
    df_new.columns = df.columns
    # Add time
    df_new = df_new.drop(columns=['@'])
    if   (tunit == 'ns'): df_new.insert(0, "@", df_new.index*dt*1000) # Convert from ns to ps
    elif (tunit == 'ps'): df_new.insert(0, "@", df_new.index*dt)
    else: print("Check time conversion."); exit()

    # Add clocks
    for i in lanes:
        # InputBlock
        path_simulation = "/top/tb/TELL40/data_proc/InputBlock_{}/".format(i)
        if (block == "InputBlock"): df_new[path_simulation+"input_data_processing_clock X"] = [1 if (x % 2) == 0 else 0 for x in df_new.index]
        # SpillOver
        path_simulation = "/top/tb/TELL40/data_proc/SpillOver_{}/".format(i)
        if (block == "SpillOver"): df_new[path_simulation+"DP_clock X"] = [1 if (x % 2) == 0 else 0 for x in df_new.index]
        # LaneBuilder
        path_simulation = "/top/tb/TELL40/data_proc/InputBlock_{}/".format(i)
        if (block == "LaneBuilder"): df_new[path_simulation+"CLKOut X"] = [1 if (x % 2) == 0 else 0 for x in df_new.index]
    # OutputBlock
    path_simulation = "/top/tb/TELL40/data_proc/OutputBlock/"
    if (block == "OutputBlock"): df_new[path_simulation+"TELL40_output_clock X"] = [1 if (x % 2) == 0 else 0 for x in df_new.index]

    # Previous values
    for name, values in df_new.items():
        if (name != "@"):
            df_new[name + " previous"] = df_new[name].shift(1)

    # Prepare Questa event list file
    file = open(newfilename, "w") 
    for index, row in df_new.iterrows():
        if (index == 0):
            file.write("@0 +0\n")
            for name, values in df_new.items():
                if (name != "@" and "previous" not in name):
                    file.write(str(name)+"\n")
        else:
            file.write("@"+str(int(row["@"]))+" +0\n")
            for name, values in df_new.items():
                if (name != "@" and "previous" not in name): 
                    if (row[name] != row[name + " previous"]):
                        name_list = name.split(' ',1)
                        countx = name_list[1].count('X')
                        data_r = str(row[name]).strip()
                        data_v = name_list[1]
                        for i in range (0,countx):
                            data_v = data_v.replace("X", data_r[i], 1)
                        file.write(name_list[0] + " " + data_v + "\n")
    file.close()
    print('Done!')
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert STP CSV file to Questa event list.')
    parser.add_argument('-i', '--inputfilename', dest='filename', default='STP_Data/ut.csv',
                        help='STP CSV input file. Default is STP_Data/ut.csv')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='STP_Decoded_Data/list.lst',
                        help='Questa event list output file. Default is STP_Decoded_Data/list.lst')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports (obsolete), 2x4eports, 2x5eports. The 2x3eports case has 4 ASICs per Lane: Use 4x3eports option.')
    parser.add_argument('-l', '--lanes', dest='lanes', default='012345',
                        help='Lanes of interest for the STP CSV file. Default is 012345')
    parser.add_argument('-b', '--block', dest='block', default='InputBlock', choices=['InputBlock','SpillOver','LaneBuilder','OutputBlock'],
                        help='Block of interest for the STP CSV file. Default is InputBlock')
    args         = parser.parse_args()
    filename     = args.filename
    newfilename  = args.newfilename
    scenario     = args.scenario
    lanes        = list(map(int, list(args.lanes)))
    block        = args.block
    print(args)
    if (scenario == '4x3eports'): nASICs = 4; bitwidth = 24; sink = "{24'hXXXXXX 24'hXXXXXX 24'hXXXXXX 24'hXXXXXX}"
    if (scenario == '2x3eports'): nASICs = 2; bitwidth = 24; sink = "{24'hXXXXXX 24'hXXXXXX}"
    if (scenario == '2x4eports'): nASICs = 2; bitwidth = 32; sink = "{32'hXXXXXXXX 32'hXXXXXXXX}"
    if (scenario == '2x5eports'): nASICs = 2; bitwidth = 40; sink = "{40'hXXXXXXXXXX 40'hXXXXXXXXXX}"
    print ("Processing STP CSV file...")
    main()
