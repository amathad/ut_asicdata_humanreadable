#!/bin/python
import numpy as np
import argparse
import warnings
from math import ceil
from util import *
import summarize_CSV

# Input: filename of the event list Questa file (!! NOT TABULAR LIST!!)
#        t0: moment when we start saving data
# Output: a dictionary of dictionaries.
#   example: mysignal['/top/tb/.../clk'][1234000]
#   will give you the value of signal /.../clk at time 1234000
def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split(" ",1)
            mysignal[name]={}
            mysignal[name][0]=value
            #print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split(" ",1)
                    if value_tmp.count("h") == 1:
                        [base, value] = value_tmp.split("h",1)
                    elif value_tmp.count("h") > 1:
                        value_tmp = value_tmp[1:-1]
                        [base, value] = value_tmp.split("h",1)
                        value = value.replace(base+"h","")
                    else:
                        value=value_tmp

                    mysignal[name][moment]=value
            line = myfile.readline().strip('\n')
    return mysignal

# Inputs:
#   thisSignal: a dictionary of {time: value}, for instance mySignal['/.../clk']
#   moment: an integer time, doesn't need to be in the dictionary
# Output:
#   The value of the signal at the specified time, extrapolated from the changes
# Example:
#   findSignalValue(mySignal['/.../clk'], 12345000) will give you the value of clk at that time

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

# Inputs:
#   sampledSignal: what signal to sample, [time, value]
#   clockSignal: what signal triggers the sampling (clk), [time, value]
#   enableSignal: what signal enables the sampling, [time, value]
#           -> 'none' disables this feature and will always sample
#
# Output:
#   a list of (time, value) of the sampled signal
#

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def associateBXID(BXID, Signal):
    BXID_cnt = 0
    associatedSignals = {}
    last_bxid = 0
    for s in Signal:
        timeStamp=s
        values=Signal[s]
        bxid=findSignalValue(BXID, timeStamp)  # hex
        #if bxid == 0xDEB:
        if int(bxid, base=16) < last_bxid:
            BXID_cnt += 1
        bxid_extended = str(BXID_cnt) + bxid
        if bxid_extended in associatedSignals:
            associatedSignals[bxid_extended]+= [values]
        else:
            associatedSignals[bxid_extended]=[values]
        
        last_bxid=int(bxid, base=16)
    return associatedSignals

def join_HL(H, L):
    tmp={}
    for (h, l) in zip(H, L):
        tmp[h]=H[h]+'_'+L[l]
    return tmp

def main():
    EventFIFO_H_In_name_01       ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_01/EventFIFO_H_In'
    EventFIFO_L_In_name_01       ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_01/EventFIFO_L_In'
    EventFIFO_H_wrreq_name_01    ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_01/EventFIFO_H_wrreq'
    EventFIFO_L_wrreq_name_01    ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_01/EventFIFO_L_wrreq'
    StartProcessing_01_name      ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/StartProcessing_01'

    EventFIFO_H_In_name_23       ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_H_In'
    EventFIFO_L_In_name_23       ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_L_In'
    EventFIFO_H_wrreq_name_23    ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_H_wrreq'
    EventFIFO_L_wrreq_name_23    ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_23_flavour/DP_MiniLB_23/EventFIFO_L_wrreq'
    StartProcessing_23_name      ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/StartProcessing_23'

    EventFIFO_H_In_name_0123     ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_0123/EventFIFO_H_In'
    EventFIFO_L_In_name_0123     ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_0123/EventFIFO_L_In'
    EventFIFO_H_wrreq_name_0123  ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_0123/EventFIFO_H_wrreq'
    EventFIFO_L_wrreq_name_0123  ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/DP_MiniLB_0123/EventFIFO_L_wrreq'
    StartProcessing_0123_name    ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/StartProcessing_0123'

    CLKOut_name                  ='/top/tb/TELL40/data_proc/InputBlock_'+lane+'/CLKOut'
    BXID_Layer1_name             ='/top/tb/TELL40/data_proc/InputBlock_'+lane+'/EventFIFO/BXID_Layer1'
    BXID_Layer2_name             ='/top/tb/TELL40/data_proc/LaneBuilder_'+lane+'/BXID_Layer2'

    mySignals = read_questa(qevtfilename, int(dtime))

    #Sampling layer 1
    EventFIFO_H_In01     = sampleSignal(mySignals[EventFIFO_H_In_name_01], mySignals[CLKOut_name], mySignals[EventFIFO_H_wrreq_name_01])
    EventFIFO_L_In01     = sampleSignal(mySignals[EventFIFO_L_In_name_01], mySignals[CLKOut_name], mySignals[EventFIFO_L_wrreq_name_01])
    EventFIFO_H_In23     = sampleSignal(mySignals[EventFIFO_H_In_name_23], mySignals[CLKOut_name], mySignals[EventFIFO_H_wrreq_name_23])
    EventFIFO_L_In23     = sampleSignal(mySignals[EventFIFO_L_In_name_23], mySignals[CLKOut_name], mySignals[EventFIFO_L_wrreq_name_23])
    BXID_Layer1          = sampleSignal(mySignals[BXID_Layer1_name],       mySignals[CLKOut_name], mySignals[StartProcessing_01_name])

    #Associating the parts with BXIDs
    EventFIFO_H01_BXID   = associateBXID(BXID_Layer1,EventFIFO_H_In01)
    EventFIFO_L01_BXID   = associateBXID(BXID_Layer1,EventFIFO_L_In01)
    EventFIFO_H23_BXID   = associateBXID(BXID_Layer1,EventFIFO_H_In23)
    EventFIFO_L23_BXID   = associateBXID(BXID_Layer1,EventFIFO_L_In23)

    #Sampling layer 2
    EventFIFO_H_In0123   = sampleSignal(mySignals[EventFIFO_H_In_name_0123], mySignals[CLKOut_name], mySignals[EventFIFO_H_wrreq_name_0123])
    EventFIFO_L_In0123   = sampleSignal(mySignals[EventFIFO_L_In_name_0123], mySignals[CLKOut_name], mySignals[EventFIFO_L_wrreq_name_0123])
    BXID_Layer2          = sampleSignal(mySignals[BXID_Layer2_name],         mySignals[CLKOut_name], mySignals[StartProcessing_0123_name])

    #Associating the parts with BXIDs
    EventFIFO_H0123_BXID = associateBXID(BXID_Layer2,EventFIFO_H_In0123)
    EventFIFO_L0123_BXID = associateBXID(BXID_Layer2,EventFIFO_L_In0123)

    dataFromFiles = {}
    dataFromFiles_sorted = []
    
    #Reverse the list
    ASIC_readouts_clean.reverse()

    status_asics = 1
    BXID_cnt = 0 
    for event_i in range (0, shortest_list):
        bxid = (hex(ASIC_readouts_clean[0][event_i].bxid))
        bxid = bxid[2:].upper() #Gets rid of the '0x' and makes it upper case.
        bxid = '00' + bxid
        bxid = bxid[-3:]
        bxid_extended = str(BXID_cnt) + bxid
        if int(bxid, base=16) == 0xDEB:
            BXID_cnt += 1
        if bxid_extended in EventFIFO_H0123_BXID.keys(): #Done only if we have data for it
            asic_names = ''
            for i in range(0,len(ASIC_readouts)): asic_names = asic_names + ASIC_readouts_clean[i][event_i].name + '/'
            asic_names = asic_names[:-1]
            if ('NP' in asic_names):
                data_ASIC = []
                for i in range(0,len(ASIC_readouts)):
                    data_ASIC.append(str(conv_binTohex(ASIC_readouts_clean[i][event_i].data)))
                    data_ASIC[i] = data_ASIC[i][2:].upper()
                    if data_ASIC[i] == '0': data_ASIC[i] = ''
                    else: data_ASIC[i]= str(i) + (' ' + str(i)).join(data_ASIC[i][j:j+3] for j in range(0,len(data_ASIC[i]),3))
                dataFromFiles[bxid_extended]=' '.join(''.join(i) for i in data_ASIC)
                dataFromFiles[bxid_extended]=' '.join(dataFromFiles[bxid_extended].split()) #Gets rid of the double spaces when there is an empty slot in the middle    
                if ((len(dataFromFiles[bxid_extended])+1) % 10 != 0): dataFromFiles[bxid_extended] = ' '.join([dataFromFiles[bxid_extended], '0000'])
                dataFromFiles[bxid_extended] = dataFromFiles[bxid_extended].split()
                dataFromFiles[bxid_extended]
                EventFIFO_total = [val for pair in zip (EventFIFO_L0123_BXID[bxid_extended], EventFIFO_H0123_BXID[bxid_extended]) for val in pair] #Interleaves the two parts
                if EventFIFO_total != dataFromFiles[bxid_extended]:
                    print("BXID: ", bxid_extended[1:], "failed! CSV expected ", dataFromFiles[bxid_extended], " but simulator said ", EventFIFO_total)
                    status_asics = 0
                else: print("BXID: ", bxid_extended[1:], " OK!")
            if ('NZS' in asic_names):
                print ("BXID: ", bxid_extended[1:])
                EventFIFO_total = [val for pair in zip (EventFIFO_H0123_BXID[bxid_extended], EventFIFO_L0123_BXID[bxid_extended]) for val in pair]
                EventFIFO_total = ''.join(EventFIFO_total)
                EventFIFO_total = bin(int(EventFIFO_total,16))[2:].zfill(len(ASIC_readouts)*(8+24+128*8))
                EventFIFO_total_asics = [EventFIFO_total[start:start+8+24+128*8] for start in range(0, len(EventFIFO_total), 8+24+128*8)]
                EventFIFO_total_asics = [x for x in EventFIFO_total_asics if int(x,2) != 0] # Remove BufferFullN
                for i in range(0,len(EventFIFO_total_asics)):
                    asic_sim = EventFIFO_total_asics[i]
                    asic_id = int(asic_sim[:8],2)
                    print ("  ASIC",asic_id,":",sep='')
                    if (asic_id > len(ASIC_readouts)-1):
                        print ("  ASIC_ID failed! ASIC_ID cannot be larger than",len(ASIC_readouts)-1)
                        status_asics = 0
                        continue
                    # Simulation
                    asic_hits_sim = asic_sim[32:]
                    asic_hits_sort_sim = [asic_hits_sim[start:start+32] for start in range(0, len(asic_hits_sim), 32)]
                    asic_hits_sort_sim.reverse()
                    asic_hits_sort_sim = ''.join(asic_hits_sort_sim)
                    asic_hits_sort_list_sim = [asic_hits_sort_sim[start:start+8] for start in range(0, len(asic_hits_sort_sim), 8)]
                    for j in range(0,len(asic_hits_sort_list_sim)): asic_hits_sort_list_sim[j] = asic_hits_sort_list_sim[j][2:] # Remove padding
                    # Expected
                    asic_exp = ASIC_readouts_clean[asic_id][event_i].data
                    if (asic_sim[8:32] != asic_exp[:24]):
                        print ("    NZS Header failed! CSV expected ", conv_binTohex(asic_exp[:24]), " but simulator said ", conv_binTohex(asic_sim[8:32]))
                        status_asics = 0
                    else: print ("    NZS Header  OK!")
                    asic_hits_exp = asic_exp[24:]
                    asic_hits_sort_list_exp = [asic_hits_exp[start:start+6] for start in range(0, len(asic_hits_exp), 6)]
                    asic_hits_sort_list_exp.reverse()
                    for j in range(0,len(asic_hits_sort_list_exp)):
                        if (asic_hits_sort_list_exp[j] != asic_hits_sort_list_sim[j]):
                            print ("    Channel", j, "failed! CSV expected ", asic_hits_sort_list_exp[j], " but simulator said ", asic_hits_sort_list_sim[j])
                            status_asics = 0
                        else: print ("    Channel", j, " OK!")
    if (status_asics == 0): print ("Status: *Error*")
    if (status_asics == 1): print ("Status: All OK!")
    print('Done!')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Summary of humand-redable UT fiber input and read Questa data. The numbering of ASICs in the summary file goes from right to left e.g. ASIC3/ASIC2/ASIC1/ASIC0.')
    parser.add_argument('-i', '--inputfilename', dest='filename', default='Fiber_Decoded_Data/UTaX4a5_ASIC{0-3}_decoded.csv',
                        help='Summary of human readable files for ASICs. The ASICID should be included in the name e.g. python summarize_CSV.py -i Fiber_Decoded_Data/UTaX4a5_ASIC{0-3}_decoded.csv.')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5.csv',
                        help='Summary file. Default is Fiber_Decoded_Data/UTaX4a5.csv')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports (obsolete), 2x4eports, 2x5eports. The 2x3eports case has 4 ASICs per Lane: Use 4x3eports option.')
    parser.add_argument('-q', '--questafilename', dest='qevtfilename', default='Questa_Data/list_UTaX4a5.lst',
                        help='Filename of the event list Questa file. Default is Questa_Data/list_UTaX4a5.lst')
    parser.add_argument('-l', '--lane', dest='lane', default='0',
                        help='Lane of interest for the event list Questa file. Default is 0')
    parser.add_argument('-t', '--dtime', dest='dtime', default='119380000',
                        help='Specify the time we start saving data in ps. Default is 119380000')
    args         = parser.parse_args()
    filename     = args.filename
    newfilename  = args.newfilename
    scenario     = args.scenario
    qevtfilename = args.qevtfilename 
    lane         = args.lane
    dtime        = args.dtime
    print(args)
    ASIC_readouts_clean, ASIC_readouts, shortest_list = summarize_CSV.main(filename=filename, newfilename=newfilename, scenario=scenario, init_bxid=0x100)
    print ("Processing Questa file...")
    main()
