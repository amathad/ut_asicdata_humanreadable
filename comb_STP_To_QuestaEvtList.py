import pandas as pd
from collections import defaultdict
import argparse
import glob

def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split(" ",1)
            mysignal[name]={}
            mysignal[name][0]=line
            #print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split(" ",1)
                    if value_tmp.count("h") == 1:
                        [base, value] = value_tmp.split("h",1)
                    elif value_tmp.count("h") > 1:
                        value_tmp = value_tmp[1:-1]
                        [base, value] = value_tmp.split("h",1)
                        value = value.replace(base+"h","")
                    else:
                        value=value_tmp

                    mysignal[name][moment]=line
            line = myfile.readline().strip('\n')
    return mysignal

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def main():
    # Read input files
    mySignals = defaultdict(dict)
    for i in filename:
        mySignals.update(read_questa(i,0))
    
    # Sample signals
    minStep = 1e20
    minTime = 1e20
    for d in mySignals.keys():
        if ('clock' in d or 'CLKOut' in d):
            secKey = [i for i in mySignals[d].keys()][1]
            if (secKey < minStep): minStep = secKey; clock_keys = list(mySignals[d].keys())
            lstKey = [i for i in mySignals[d].keys()][-1]
            if (lstKey < minTime): minTime = lstKey
    clock_keys[:] = [x for x in clock_keys if x <= minTime]
    clock_values = ['1']*len(clock_keys)
    clock = dict(zip(clock_keys,clock_values))
    for d in mySignals.keys():
        mySignals[d] = sampleSignal(mySignals[d], clock, 'none')

    # Import signals to dataframe
    df = pd.DataFrame(mySignals)

    # Previous values
    for name, values in df.items():
        df[name + " previous"] = df[name].shift(1)
    
    # Save a single list
    file = open(newfilename, "w") 
    for index, row in df.iterrows():
        if (int(index) > minTime): continue
        file.write("@"+str(int(index))+" +0\n")
        for name, values in df.items():
            if ("previous" not in name): 
                if (row[name] != row[name + " previous"]):
                    file.write(row[name] + "\n")
    file.close()
    print('Done!')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Combine STP Questa event lists to a single list.')
    parser.add_argument('-i', '--inputfilename', dest='filename', default='STP_Decoded_Data/list*.lst',
                        help='Input STP Questa event lists with a wildcard. Default is "STP_Decoded_Data/list*.lst"')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='STP_Decoded_Data/list.lst',
                        help='Questa event list output file. Default is STP_Decoded_Data/list.lst')
    args         = parser.parse_args()
    filename     = glob.glob(args.filename)
    newfilename  = args.newfilename
    print(args)
    print ("Processing STP Questa event lists...")
    main()
