#!/bin/python
import pandas as pd
import argparse
import warnings

def main():
    df = pd.read_csv("AsicToFiberMapping.csv")
    dfs = df[df["Stave"]==staveid]
    dfs = dfs.drop_duplicates(subset=['PEPI', 'Flex', 'Hybrid'], keep='last').reset_index(drop=True)
    print ("\nSensors in stave:")
    print (dfs[['PEPI', 'Flex', 'Hybrid']])
    if (sensors): exit()
    dfa = df[(df["Stave"]==staveid) & (df["PEPI"]==pepiid) & (df["Flex"]==flexid) & (df["Hybrid"]==hybridid)]
    dfa = dfa.reset_index(drop=True);
    dfa.rename(columns = {'BP variant (alpha/beta/gamma)':'BP variant',
                          'GBTx channels (GBT frame bytes)':'GBTx channels'}, inplace = True)
    print ("\nASICs in sensor:")
    print (dfa[["DCB index", "BP variant", "GBTx index", "GBTx channels", "ASIC index"]])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ASICs to fiber mapping for a specific sensor based on AsicToFiberMapping.csv. \
                                                  The naming scheme of GBTs is layer (e.g. UTaX), then DCB number, backplane letter (a for alpha, b for beta, g for gamma) and GBT number.')
    parser.add_argument('-s', '--staveid', dest='staveid', default='UTaX_4C', help='The Stave ID of the sensor. Default is UTaX_4C.')
    parser.add_argument('-b', '--sensors', dest='sensors', default=False, action=argparse.BooleanOptionalAction, help='Print only the PEPI, Flex and Hybrid IDs for all sensors in stave.')
    parser.add_argument('-p', '--pepiid', dest='pepiid', default='IP-Bottom-C', help='The PEPI ID of the sensor. Default is IP-Bottom-C.')
    parser.add_argument('-f', '--flexid', dest='flexid', default='X-0-S', help='The Flex ID of the sensor. Default is X-0-S.')
    parser.add_argument('-r', '--hybridid', dest='hybridid', default='P3', help='The Hybrid ID of the sensor. Default is P3.')    
    args     = parser.parse_args()
    staveid  = args.staveid
    sensors  = args.sensors
    pepiid   = args.pepiid
    flexid   = args.flexid
    hybridid = args.hybridid
    print(args)
    main()
