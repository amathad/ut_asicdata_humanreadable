#!/bin/python
import pandas as pd
import argparse
import warnings
import random
import math

# Ax ≡ B (MOD C)
# A=12bits (Idle), B=-nbits,
# C=bitwidth (24, 32, 40 bits)
def congru(a,b,c):
    for i in range(0,c):
        if ((a*i - b)%c) == 0: fres = i; break
    return fres

def main():
    nbits_list = []
    for i in range (0,nASICS):
        nbits = 0
        inputfile  = inputfiles.replace("*", str(i))
        outputfile = outputfiles.replace("*", str(i))
        with open(inputfile, "r") as filestream:
            with open(outputfile, "w") as filestreamtwo:
                for line in filestream:
                    if ("IncompleteData" in line): fulldata = False
                    else:                          fulldata = True
                    currentline = line.split(",")
                    if (currentline[2] == "Sync" and fulldata): nbits += 24; filestreamtwo.write(line)
                    if (currentline[2] == "HeaderOnly" and fulldata): nbits += 12; filestreamtwo.write(line)
                    if (currentline[2] == "Idle\n" and fulldata): nbits += 12; filestreamtwo.write(line);
                    if (currentline[2] == "Normal" and fulldata): nbits += 12 + 12*int(currentline[8]); filestreamtwo.write(line)
                    if (currentline[2] == "BxVeto" and fulldata): nbits += 12; filestreamtwo.write(line)
                    if (currentline[2] == "BufferFull" and fulldata): nbits += 12; filestreamtwo.write(line)
                    if (currentline[2] == "BufferFullN" and fulldata): nbits += 12; filestreamtwo.write(line)
                    if (currentline[2] == "NZS" and fulldata): nbits += 12 + 24 + 6*128; filestreamtwo.write(line)
                    if (currentline[2] == "BusyEvent" and fulldata): nbits += 12; filestreamtwo.write(line)
                    if (currentline[2] == "PacketName" and fulldata): filestreamtwo.write(line)
        nbits_list.append(nbits)
    print ("Total number of bits for each ASIC:", nbits_list)
    maxbits = max(nbits_list) + 12*congru(12,-max(nbits_list),bitwidth)
    print ("All files will have",maxbits,"bits by adding Idle packets.")
    for i in range (0,nASICS):
        outputfile = outputfiles.replace("*", str(i))
        file_object = open(outputfile, 'a')
        for j in range (0,int((maxbits-nbits_list[i])/12)):
            file_object.write("100,3,Idle\n")
        file_object.close()
    print('Done!')
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Remove incomplete data from humand-redable UT fiber input files and add Idle packets to synchronize all ASICs.')
    parser.add_argument('-i', '--inputfilename', dest='inputfiles', default='Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_decoded.csv',
                        help='Human readable files for ASICs. Denote the ASICID with an asterisk in the name'
                             ' e.g. python RemoveIncompleteData.py -i "Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_decoded.csv".')
    parser.add_argument('-o', '--outputfilename', dest='outputfiles', default='Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_corrected.csv',
                        help='Output file. Default is "Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_corrected.csv". Use an asterisk to denote the different ASICs.')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    args         = parser.parse_args()
    inputfiles   = args.inputfiles
    outputfiles  = args.outputfiles
    scenario     = args.scenario
    print(args)
    if (scenario == '4x3eports'): nASICS = 4; bitwidth = 24
    if (scenario == '2x3eports'): nASICS = 2; bitwidth = 24
    if (scenario == '2x4eports'): nASICS = 2; bitwidth = 32
    if (scenario == '2x5eports'): nASICS = 2; bitwidth = 40
    main()
