#!/bin/python
import argparse
from math import ceil
from util import *

def choose_one(hexs,decs, bitwidth):
    s = None
    if (hexs == '' and decs != ''):
        s = int(decs)
    elif (hexs != '' and decs == ''):
        s = conv_binTonum(conv_hexTobin(hexs, bitwidth = bitwidth))
    elif (hexs != '' and decs != ''):
        s1 = int(decs)
        s2 = conv_binTonum(conv_hexTobin(hexs, bitwidth = bitwidth))
        if s1 == s2:
            s = s1
        else:
            raise Exception('The hex and int of the passed', hexs, 'and', decs, 'do not match! Check')
    elif (hexs == '' and decs == ''):
        raise Exception('Both the hext and int are empty. Check!')

    return s

def get_bxid(isTELL40input, lsplit, interation_bxid, prev_bxid):
    bxid       = None
    if not isTELL40input:
        bxid   = conv_numTobin(choose_one(lsplit[3], lsplit[4], '12b'), '12b')
        #print(conv_binTonum(bxid))
    else:
        #get the 4 bits, converting it into actual value
        cbxid = choose_one(lsplit[3], lsplit[4], '4b')
        bxid  = cbxid + interation_bxid + conv_binTonum('1111') * interation_bxid + init_bxid
        if cbxid == 15: interation_bxid += 1
        bxid = conv_numTobin(bxid, '12b')

    #set the prev_bxid, if not raise exception
    if prev_bxid is None:
        prev_bxid = conv_binTonum(bxid)
    else:
        if prev_bxid + 1 != conv_binTonum(bxid): raise Exception('The current bxid', conv_binTonum(bxid), 'does not match the prev bxid', prev_bxid)
        prev_bxid = conv_binTonum(bxid)

    return bxid, interation_bxid, prev_bxid


def read_csv(Filename):
    print('Reading in file', Filename)
    readouts = []
    with open(Filename, 'r') as myfile:
        print('Skipping the header')
        _ = [myfile.readline() for x in range(1)]
        line = myfile.readline()
        line = line.strip('\n')
        prev_bxid = None
        interation_bxid = 0
        count= 0
        while(line != ''):
            lsplit = line.split(',')
            lsplit = lsplit[:-1] + [(lsplit[-1]).replace('\r', '')]
            lsplit = [ls.replace(' ', '') for ls in lsplit]
            #print(lsplit)
            linenum    = int(lsplit[0]) #dummy variable
            zerosafter = int(lsplit[1])
            name       = lsplit[2]
            if name != 'Idle':
                bxid, interation_bxid, prev_bxid = get_bxid(isTELL40input, lsplit, interation_bxid, prev_bxid)
                dtp = None
                if name == 'Sync':
                    dtp     = SyncPacket(name, bxid)
                    dtp.linenum = linenum
                    dtp.zerosafter = zerosafter #NB it is 25bit, since the last bit is the data value which in this case is zero
                elif name == 'Normal':
                    length         = conv_numTobin(choose_one(lsplit[7], lsplit[8], '6b'), '6b')
                    dtp            = DataPacket_HR(name, bxid, length)
                    dtp.linenum    = linenum
                    dtp.zerosafter = zerosafter #NB it is 25bit, since the last bit is the data value which in this case is zero
                    hitdata = ''
                    if conv_binTonum(length) != 0:
                        for htd in lsplit[9:]:
                            hd = htd.split('(')[-1]
                            if '(' in htd: #if there is (stripid:adcval) format use that
                                #print(hd)
                                strippedhd = ((hd.replace('(','')).replace(')','')).split(':')
                                sid = strippedhd[0]
                                adc = strippedhd[1]
                                #print(sid, adc)
                                hitdata += conv_numTobin(int(sid), '7b')+conv_numTobin(int(adc), '5b')
                            else: #or else use hex
                                hd       = htd.split('(')[0]
                                hitdata += conv_hexTobin(hd, bitwidth = str(12)+'b')

                    dtp.data = hitdata
                    #print(hitdata)
                elif name == 'NZS':
                    dtp            = DataPacket_HR(name, bxid)
                    dtp.linenum    = linenum
                    dtp.zerosafter = zerosafter #NB it is 25bit, since the last bit is the data value which in this case is zero
                    #fill hitdata
                    hitdata = conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)
                    if len(hitdata) > 24 + 6 * 128:
                        print('INFO: Truncating the NZS data packet to fit to', 24 + 6 * 128, '. If you did not intend this check!')
                        hitdata = hitdata[:24 + 6 * 128]

                    dtp.data = hitdata
                    #print(hitdata)
                else:
                    dtp            = DataPacket_HR(name, bxid)
                    dtp.linenum    = linenum
                    dtp.zerosafter = zerosafter #NB it is 25bit, since the last bit is the data value which in this case is zero

                print('linenum',dtp.linenum,'zeroafter',dtp.zerosafter,'name',dtp.name,'bxid',dtp.bxid,'parity',dtp.parity,\
                      'flag',dtp.flag,'length',dtp.length,'data',dtp.data)
                readouts += [dtp]
            #else:
            #    bxid, interation_bxid, prev_bxid = get_bxid(isTELL40input, lsplit, interation_bxid, prev_bxid)
                
            line = myfile.readline()
            line = line.strip('\n')
            #if count == 3: break

    return readouts

def write_datafile(readouts, Newfilename, Scenario):
    asicwidth = None
    if Scenario == '2x4eports':
        asicwidth = 32
    elif Scenario == '2x5eports':
        asicwidth = 40
    else:
        asicwidth = 24
        
    lines = []
    for rd in readouts:
        line = None
        if rd.name == 'Sync':
            line = rd.bxid+rd.data
        elif rd.name == 'Normal' or rd.name == 'NZS':
            line = rd.bxid+rd.parity+rd.flag+rd.length+rd.data
        else:
            line = rd.bxid+rd.parity+rd.flag+rd.length

        #print(line)
        for i in range(int(ceil(len(line)/float(asicwidth)))):
            nline = asicwidth - len(line[i*asicwidth:i*asicwidth+asicwidth]) 
            if nline == 0:
                lines += [line[i*asicwidth:i*asicwidth+asicwidth] + '1'] #last 1 is the data value
            else:
                lines += [line[i*asicwidth:i*asicwidth+asicwidth] + nline * '0' + '1'] #last 1 is the data value

            if len(lines[-1]) != asicwidth+1: raise Exception('Bit length not equal to ',asicwidth+1,'b. Check!')

        lines += rd.zerosafter*[(asicwidth+1)*'0']
        #print(lines)

    #for line in lines: print(line)
    with open(Newfilename, 'w') as txtfile: 
        print('Writing the data output file with name', Newfilename)
        s = '\n'.join(lines)
        txtfile.write(s)

def main():
    readouts = read_csv(filename)
    #print(len(readouts))
    write_datafile(readouts, newfilename, scenario)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert the csv file to TELL40 output txt file after data processing step')
    parser.add_argument('-i'  , '--inputfilename' , dest='filename'   , default = 'ASIC0.csv', help='File in csv format (include absolute or relative path) containing TELL40 output. Default is ASIC0.csv')
    parser.add_argument('-o'  , '--outputfilename', dest='newfilename', default = 'ASIC_Coded_Data/ASIC0.txt', help='Name of binary output file to be written. Default is ASIC_Coded_Data/ASIC0.txt')
    parser.add_argument('-s'  , '--scenario'      , dest='scenario'   , default ='4x3eports'  , help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    parser.add_argument('-t'  , '--istell40input' , dest='isTELL40input',type=str2bool, nargs='?', const=True, default=False, help='Is this TELL40 *input* (True) or TELL40 *output* (False). Default is False.')
    parser.add_argument('-b'  , '--init_bxid'     , dest='init_bxid'    ,default='0', help='If this is TELL40 what is the initial bunching crossing ID? The default in 0.')
    args          = parser.parse_args()
    filename      = args.filename
    newfilename   = args.newfilename
    scenario      = args.scenario
    isTELL40input = args.isTELL40input
    init_bxid     = args.init_bxid
    print(args)
    main()
