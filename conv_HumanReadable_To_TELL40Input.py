#!/bin/python
import numpy as np
import argparse
import warnings
from math import ceil
from util import *

def read_csv(Filename):
    readouts = []
    print_sync_info = False
    with open(Filename, 'r') as myfile:
        print('Skipping the header')
        _ = [myfile.readline() for x in range(1)]
        line = myfile.readline().strip('\n')
        while(line != ''):
            lsplit = line.split(',')
            lsplit = lsplit[:-1] + [(lsplit[-1]).replace('\r', '')]
            lsplit = [ls.replace(' ', '') for ls in lsplit]
            print(lsplit)
    
            linenum = int(lsplit[0])
            zerosafter = int(lsplit[1])
            name = lsplit[2]
            dtp = None
            if name != 'Idle':
                bxid = conv_hexTobin(lsplit[3] , bitwidth = '4b')
                if len(bxid) != 4: 
                    #print('Bxid is trimmed!')
                    bxid = bxid[-4:] #caution

                if name == 'Sync':
                    bxid_sync = conv_hexTobin(lsplit[3] , bitwidth = '12b')
                    if len(bxid_sync) != 12: raise Exception('Bxid sync is not of length 12b. Check!')

                    sync_patt_fixed = 'fff' #'caa' 
                    if not print_sync_info:
                        print('Note the sync pattern being used is: fff')
                        print_sync_info = True

                    dtp = SyncPacket(name, bxid_sync, sync_patt_fixed = sync_patt_fixed)
                elif name == 'Normal':
                    length  = conv_numTobin(int(lsplit[8]), '6b')
                    if len(length) != 6: raise Exception('Length is not of length 6b. Check!')

                    dtp     = DataPacket_HR(name, bxid, length)
                    hitdata = ''
                    if conv_binTonum(length) != 0:
                        if 'IncompleteData' not in lsplit[9]:
                            for htd in lsplit[9:]:
                                hd = htd.split('(')[-1]
                                if '(' in htd: #if there is (stripid:adcval) format use that
                                    #print(hd)
                                    strippedhd = ((hd.replace('(','')).replace(')','')).split(':')
                                    sid = strippedhd[0]
                                    adc = strippedhd[1]
                                    #print(sid, adc)
                                    if len(conv_numTobin(int(sid), '7b')) != 7 or len(conv_numTobin(int(adc), '5b')) != 5:
                                        raise Exception('The ASICID and ADC are not of correct width i.e. 7b and 5b, respectively. Check!')

                                    hitdata += conv_numTobin(int(sid), '7b')+conv_numTobin(int(adc), '5b')
                                else: #or else use hex
                                    hd       = htd.split('(')[0]
                                    if len(conv_hexTobin(hd, bitwidth = '12b')) != 12:
                                        raise Exception('The hit data is not of length 12b in normal packet. Check!')

                                    hitdata += conv_hexTobin(hd, bitwidth = '12b')
                        else:
                            print('INFO: The length of the normal packet is {0}, but data packet says NoData. Maybe the data has overflown!.'.format(conv_binTonum(length)))
                            hitdata = (lsplit[9].split('(')[1]).split(')')[0]
    
                    dtp.data = hitdata
                    #print(hitdata)
                elif name == 'NZS':
                    dtp  = DataPacket_HR(name, bxid)
                    hitdata = ''
                    if 'IncompleteData' not in lsplit[9]:
                        if len(conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)) != 24 + 6 * 128:
                            raise Exception('The hit data for NZS packet is not of correct lenth. Check!')

                        hitdata  = conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)

                        if len(hitdata) > 24 + 6 * 128:
                            print('INFO: Truncating the NZS data packet to fit to', 24 + 6 * 128, '. If you did not intend this check!')
                            hitdata = hitdata[:24 + 6 * 128]
                    else:
                        print('INFO: There is overflow in NZS data!')
                        hitdata = (lsplit[9].split('(')[1]).split(')')[0]
    
                    dtp.data = hitdata
                    #print(hitdata)
                else:
                    dtp  = DataPacket_HR(name, bxid)
            else:
                dtp = DataPacket('Idle', '0000', '1', '1', '110000')
    
            readouts += [dtp]
            line = myfile.readline().strip('\n')
    
    return readouts

def write_datafile(asic_readouts):
    asicwidth = None
    if scenario == '2x4eports':
        asicwidth = 32
    elif scenario == '2x5eports':
        asicwidth = 40
    else:
        asicwidth = 24

    asic_lines = []
    for asic_id, asic_readout in enumerate(asic_readouts):
        #make one long line of asic readout
        line = ''
        for rd in asic_readout:
            if rd.name == 'Sync':
                line += rd.bxid+rd.data
            elif rd.name == 'Normal' or rd.name == 'NZS':
                line += rd.bxid+rd.parity+rd.flag+rd.length+rd.data
            else:
                line += rd.bxid+rd.parity+rd.flag+rd.length

            #print(line)

        #print(line)
        #split the long line into lines according to asicwidth and at the end zero pad
        lines = []
        for i in range(int(ceil(len(line)/float(asicwidth)))):
            nline = asicwidth - len(line[i*asicwidth:i*asicwidth+asicwidth]) 
            if nline == 0:
                lines += [line[i*asicwidth:i*asicwidth+asicwidth]]
            else:
                print('CAUTION: Zero padding the end of the data packet for ASICID {0}, such that it matches the bit width {1}b!'.format(len(asic_readouts) - asic_id - 1, asicwidth))
                lines += [line[i*asicwidth:i*asicwidth+asicwidth] + nline * '0']

            if len(lines[-1]) != asicwidth: raise Exception('Bit length not equal to ',asicwidth+1,'b. Check!')

        #print(lines)
        asic_lines += [lines]

    number_of_lines = set([len(asic_l) for asic_l in asic_lines])
    if len(number_of_lines) != 1:
        raise Exception("The number of lines in all the ASIC do not match. Check!")

    #write the file
    with open(newfilename, 'w') as txtfile: 
        print('Writing the data output file with name', newfilename)
        for i in range(list(number_of_lines)[0]):
            s = 16 * '0'
            if scenario == '4x3eports':
                s += asic_lines[0][i] + asic_lines[1][i] + asic_lines[2][i] + asic_lines[3][i] + '1'
            elif scenario == '2x3eports':
                s += asic_lines[0][i] + 16 * '0' + asic_lines[1][i] + 32 * '0' + '1'
            elif scenario == '2x4eports':
                s += asic_lines[0][i] + 8 * '0' + asic_lines[1][i] + 24 * '0' + '1'
            elif scenario == '2x5eports':
                s += asic_lines[0][i] + asic_lines[1][i] + 16 * '0' + '1'

            s += '\n'
            txtfile.write(s)

def main():
    #get the file names from the input file name
    open_brackets = filename.split('{')
    prefix = open_brackets[0]
    suffix = filename.split('}')[1]
    range_indxs = open_brackets[1].split('}')[0]
    init_indx = int(range_indxs.split('-')[0])
    last_indx = int(range_indxs.split('-')[1])
    asic_files= [prefix+str(i)+suffix for i in range(init_indx, last_indx+1)]
    asic_files= list(reversed(asic_files)) #bcos they are written in a reverse order

    if scenario == '4x3eports' and len(asic_files) != 4:
        raise Exception('The scenario chosen is {0}, but only {1} passed. Need to pass 4 ASIC files!'.format(scenario, len(asic_files)))

    if scenario != '4x3eports' and len(asic_files) != 2:
        raise Exception('The scenario chosen is {0}, but only {1} passed. Need to pass 2 ASIC files!'.format(scenario, len(asic_files)))

    asic_readouts = [read_csv(asic_file) for asic_file in asic_files]
    write_datafile(asic_readouts)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert humand-redable UT fiber input to binary format.')
    parser.add_argument('-i'  , '--inputfilename' , dest='filename'   , default='Fiber_Decoded_Data/UTaX4a5_ASIC{0-3}_decoded.csv', help='Human readable files for each ASIC. The ASICID should be included in the name e.g. python conv_HumanReadable_To_TELL40Input.py -i Fiber_Decoded_Data/UTaX4a5_ASIC{0-3}_decoded.csv.')
    parser.add_argument('-o'  , '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5.txt', help='Binar file. Default is Fiber_Decoded_Data/UTaX4a5.txt')
    parser.add_argument('-s'  , '--scenario'      , dest='scenario'   , default='4x3eports'  , help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    args        = parser.parse_args()
    filename    = args.filename
    newfilename = args.newfilename
    scenario    = args.scenario
    print(args)
    main()
