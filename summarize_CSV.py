#!/bin/python
import numpy as np
import argparse
import warnings
from math import ceil
from util import *

def read_csv(Filename):
    readouts = []
    print_sync_info = False
    with open(Filename, 'r') as myfile:
        #print('Reading file ' + Filename)
        #print('Skipping the header')
        _ = [myfile.readline() for x in range(1)]
        line = myfile.readline().strip('\n')
        while(line != ''):
            lsplit = line.split(',')
            lsplit = lsplit[:-1] + [(lsplit[-1]).replace('\r', '')]
            lsplit = [ls.replace(' ', '') for ls in lsplit]
            #print(lsplit)
    
            linenum = int(lsplit[0])
            zerosafter = int(lsplit[1])
            name = lsplit[2]
            dtp = None
            if name != 'Idle':
                bxid = conv_hexTobin(lsplit[3] , bitwidth = '4b')
                if len(bxid) != 4: 
                    #print('Bxid is trimmed!')
                    bxid = bxid[-4:] #caution

                if name == 'Sync':
                    bxid_sync = conv_hexTobin(lsplit[3] , bitwidth = '12b')
                    if len(bxid_sync) != 12: raise Exception('Bxid sync is not of length 12b. Check!')

                    sync_patt_fixed = 'fff' #'caa' 
                    if not print_sync_info:
                        #print('Note the sync pattern being used is: fff')
                        print_sync_info = True

                    dtp = SyncPacket(name, bxid_sync, sync_patt_fixed = sync_patt_fixed)
                elif name == 'Normal':
                    length  = conv_numTobin(int(lsplit[8]), '6b')
                    if len(length) != 6: raise Exception('Length is not of length 6b. Check!')

                    dtp     = DataPacket_HR(name, bxid, length)
                    hitdata = ''
                    if conv_binTonum(length) != 0:
                        if 'IncompleteData' not in lsplit[9]:
                            for htd in lsplit[9:]:
                                hd = htd.split('(')[-1]
                                if '(' in htd: #if there is (stripid:adcval) format use that
                                    #print(hd)
                                    strippedhd = ((hd.replace('(','')).replace(')','')).split(':')
                                    sid = strippedhd[0]
                                    adc = strippedhd[1]
                                    #print(sid, adc)
                                    if len(conv_numTobin(int(sid), '7b')) != 7 or len(conv_numTobin(int(adc), '5b')) != 5:
                                        raise Exception('The ASICID and ADC are not of correct width i.e. 7b and 5b, respectively. Check!')

                                    hitdata += conv_numTobin(int(sid), '7b')+conv_numTobin(int(adc), '5b')
                                else: #or else use hex
                                    hd       = htd.split('(')[0]
                                    if len(conv_hexTobin(hd, bitwidth = '12b')) != 12:
                                        raise Exception('The hit data is not of length 12b in normal packet. Check!')

                                    hitdata += conv_hexTobin(hd, bitwidth = '12b')
                        else:
                            print('INFO: The length of the normal packet is {0}, but data packet says NoData. Maybe the data has overflown!.'.format(conv_binTonum(length)))
                            hitdata = (lsplit[9].split('(')[1]).split(')')[0]
    
                    dtp.data = hitdata
                    #print(hitdata)
                elif name == 'NZS':
                    dtp  = DataPacket_HR(name, bxid)
                    hitdata = ''
                    if 'IncompleteData' not in lsplit[9]:
                        if len(conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)) != 24 + 6 * 128:
                            raise Exception('The hit data for NZS packet is not of correct lenth. Check!')

                        hitdata  = conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)

                        if len(hitdata) > 24 + 6 * 128:
                            print('INFO: Truncating the NZS data packet to fit to', 24 + 6 * 128, '. If you did not intend this check!')
                            hitdata = hitdata[:24 + 6 * 128]
                    else:
                        print('INFO: There is overflow in NZS data!')
                        hitdata = (lsplit[9].split('(')[1]).split(')')[0]
    
                    dtp.data = hitdata
                    #print(hitdata)
                else:
                    dtp  = DataPacket_HR(name, bxid)
            else:
                dtp = DataPacket('Idle', '0000', '1', '1', '110000')

            readouts += [dtp]
            line = myfile.readline().strip('\n')

    return readouts

def write_csv(Newfilename, asicheaders, readouts):
    print('Writing a new file', Newfilename)
    with open(Newfilename, 'w') as csvfile: 
        #print(asicheaders)
        csvfile.write(asicheaders)
        for readout in readouts:
            BXIDHex     =readout.bxid
            Packets     =readout.names
            Lengths     =readout.lengths
            TotalLength =readout.totallengths
            Hits        =readout.data
            s = '{0:},{1:},{2:},{3:},{4:},\n'.format(BXIDHex,Packets,Lengths,TotalLength,Hits)
            #print(s)
            csvfile.write(s)

def deduce_BXID(readouts, init_bxid):
    currentBXID = init_bxid #By default it is always 0x100 
    for event in readouts:
        if event.name == 'Sync':
            currentBXID=conv_binTonum(event.bxid)+1
        elif event.name != 'Idle':
            event.bxid=currentBXID
            currentBXID+=1
            currentBXID%=3564
    return readouts

def cleanup_readouts(readouts):
    for event in readouts:
        if event.name =='HeaderOnly':
            event.name = 'HO'
            event.data = '0'
        if event.data==None:
            event.data = '0'
        if event.data=='':
            event.data = '0'
        if event.name=='Normal':
            event.name='NP'
    return readouts


class SummaryFile():
    def __init__(self, names, bxid, lengths, totallengths, hits):
        self.names        = names   
        self.bxid         = bxid   
        self.lengths      = lengths
        self.totallengths = totallengths
        self.data         = hits

def write_datafile(asic_files, newfilename, init_bxid):

    # We read all files
    ASIC_readouts = []
    ASIC_readouts_clean = []
    for asic_file in asic_files: ASIC_readouts.append(read_csv(asic_file))
    for i in range(0,len(ASIC_readouts)): ASIC_readouts[i] = deduce_BXID(ASIC_readouts[i], init_bxid)
    for i in range(0,len(ASIC_readouts)): ASIC_readouts[i] = cleanup_readouts(ASIC_readouts[i])

    # We remove Syncs
    for i in range(0,len(ASIC_readouts)): ASIC_readouts_clean.append([x for x in ASIC_readouts[i] if x.name !='Sync'])

    # We remove Idles
    for i in range(0,len(ASIC_readouts)): ASIC_readouts_clean[i] = [x for x in ASIC_readouts_clean[i] if x.name !='Idle']

    # We see which is the shortest list
    ASIC_readouts_clean_length = []
    for i in range(0,len(ASIC_readouts)): ASIC_readouts_clean_length.append(len(ASIC_readouts_clean[i]))
    shortest_list = min( ASIC_readouts_clean_length)
    for i in range(0,len(ASIC_readouts)): ASIC_readouts_clean[i] = ASIC_readouts_clean[i] [:shortest_list]

    # We check all BXIDs are equal
    for event_i in range (0, shortest_list):
        ASIC_readouts_clean_bxid = []
        for i in range(0,len(ASIC_readouts)): ASIC_readouts_clean_bxid.append(ASIC_readouts_clean[i][event_i].bxid)
        if (len(set(ASIC_readouts_clean_bxid)) == 1) != True:
            print("Error, the BXIDs do not match!")
            exit(-1)
    print("BXID Check sucessful...")

    # We prepare the file to write
    asicheaders = '{0:},{1:},{2:},{3:},{4:},\n'.format('BXIDHex','Packets','Lengths','TLen','Hits')
    readouts=[]
    for event_i in range (0, shortest_list):
        bxid         = hex(ASIC_readouts_clean[0][event_i].bxid)
        names        = ASIC_readouts_clean[0][event_i].name
        for i in range(1,len(ASIC_readouts)): names += '/' + ASIC_readouts_clean[i][event_i].name
        lengths      = str(conv_binTonum(ASIC_readouts_clean[0][event_i].length)).zfill(2)
        for i in range(1,len(ASIC_readouts)): lengths += '/' + str(conv_binTonum(ASIC_readouts_clean[i][event_i].length)).zfill(2)
        totallengths = conv_binTonum(ASIC_readouts_clean[0][event_i].length)
        for i in range(1,len(ASIC_readouts)): totallengths += conv_binTonum(ASIC_readouts_clean[i][event_i].length)
        totallengths = str(totallengths).zfill(3)
        hits         = str(conv_binTohex(ASIC_readouts_clean[0][event_i].data))
        for i in range(1,len(ASIC_readouts)): hits += '/' + str(conv_binTohex(ASIC_readouts_clean[i][event_i].data))
        this_event   = SummaryFile(names, bxid, lengths, totallengths, hits)
        readouts.append(this_event)

    # We write the file
    write_csv(newfilename, asicheaders, readouts)
    print('Done!')
    return ASIC_readouts_clean, ASIC_readouts, shortest_list 

def main(filename, newfilename, scenario, init_bxid):
    #get the file names from the input file name
    open_brackets = filename.split('{')
    prefix = open_brackets[0]
    suffix = filename.split('}')[1]
    range_indxs = open_brackets[1].split('}')[0]
    init_indx = int(range_indxs.split('-')[0])
    last_indx = int(range_indxs.split('-')[1])
    asic_files= [prefix+str(i)+suffix for i in range(init_indx, last_indx+1)]
    asic_files= list(reversed(asic_files)) #bcos they are written in a reverse order

    if scenario == '4x3eports' and len(asic_files) != 4:
        raise Exception('The scenario chosen is {0}, but only {1} passed. Need to pass 4 ASIC files!'.format(scenario, len(asic_files)))

    if scenario != '4x3eports' and len(asic_files) != 2:
        raise Exception('The scenario chosen is {0}, but only {1} passed. Need to pass 2 ASIC files!'.format(scenario, len(asic_files)))

    asic_readouts = [read_csv(asic_file) for asic_file in asic_files]
    ASIC_readouts_clean = []
    ASIC_readouts = []
    ASIC_readouts_clean, ASIC_readouts, shortest_list = write_datafile(asic_files, newfilename, init_bxid)
    return ASIC_readouts_clean, ASIC_readouts, shortest_list 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Summary of humand-redable UT fiber input. The numbering of ASICs in the summary file goes from right to left e.g. ASIC3/ASIC2/ASIC1/ASIC0.')
    parser.add_argument('-i'  , '--inputfilename' , dest='filename'   , default='Fiber_Decoded_Data/UTaX4a5_ASIC{0-3}_decoded.csv',
                        help='Summary of human readable files for ASICs. The ASICID should be included in the name e.g. python summarize_CSV.py -i Fiber_Decoded_Data/UTaX4a5_ASIC{0-3}_decoded.csv.')
    parser.add_argument('-o'  , '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5.csv', help='Summary file. Default is Fiber_Decoded_Data/UTaX4a5.csv')
    parser.add_argument('-s'  , '--scenario'      , dest='scenario'   , default='4x3eports'  , help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    parser.add_argument('-b'  , '--init_bxid'     , dest='init_bxid'  , default=0x100, help='What is the initial bunch crossing ID, needed if first packet is not a sync. Default is 0x100')
    args        = parser.parse_args()
    filename    = args.filename
    newfilename = args.newfilename
    scenario    = args.scenario
    init_bxid   = args.init_bxid
    print(args)
    main(filename, newfilename, scenario, init_bxid)
