#!/bin/python
import numpy as np
import pandas as pd
import argparse
import warnings
from math import ceil
from util import *
import os
import subprocess
import summarize_CSV

# Input: filename of the event list Questa file (!! NOT TABULAR LIST!!)
#        t0: moment when we start saving data
# Output: a dictionary of dictionaries.
#   example: mysignal['/top/tb/.../clk'][1234000]
#   will give you the value of signal /.../clk at time 1234000
def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split(" ",1)
            mysignal[name]={}
            mysignal[name][0]=value
            #print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split(" ",1)
                    if value_tmp.count("h") == 1:
                        [base, value] = value_tmp.split("h",1)
                    elif value_tmp.count("h") > 1:
                        value_tmp = value_tmp[1:-1]
                        [base, value] = value_tmp.split("h",1)
                        value = value.replace(base+"h","")
                    else:
                        value=value_tmp

                    mysignal[name][moment]=value
            line = myfile.readline().strip('\n')
    return mysignal

# Inputs:
#   thisSignal: a dictionary of {time: value}, for instance mySignal['/.../clk']
#   moment: an integer time, doesn't need to be in the dictionary
# Output:
#   The value of the signal at the specified time, extrapolated from the changes
# Example:
#   findSignalValue(mySignal['/.../clk'], 12345000) will give you the value of clk at that time

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

# Inputs:
#   sampledSignal: what signal to sample, [time, value]
#   clockSignal: what signal triggers the sampling (clk), [time, value]
#   enableSignal: what signal enables the sampling, [time, value]
#           -> 'none' disables this feature and will always sample
#
# Output:
#   a list of (time, value) of the sampled signal
#

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def associateBXID(BXID, Signal):
    BXID_cnt = 0
    associatedSignals = {}
    last_bxid = 0
    for s in Signal:
        timeStamp=s
        values=Signal[s]
        bxid=findSignalValue(BXID, timeStamp)  # hex
        #if bxid == 0xDEB:
        if int(bxid, base=16) < last_bxid:
            BXID_cnt += 1
        bxid_extended = str(BXID_cnt) + bxid
        if bxid_extended in associatedSignals:
            associatedSignals[bxid_extended]+= [values]
        else:
            associatedSignals[bxid_extended]=[values]
        
        last_bxid=int(bxid, base=16)
    return associatedSignals

def join_HL(H, L):
    tmp={}
    for (h, l) in zip(H, L):
        tmp[h]=H[h]+'_'+L[l]
    return tmp

# First bit is the sign
def conv_sbinTonum(binr):
    if (binr[0] == '0'): s =  1
    else:                s = -1
    n = conv_binTonum(binr[1:])
    return s*n

# First bit is the sign
def conv_numTosbin(num, bitwidth):
    b = conv_numTobin(abs(num), bitwidth-1)
    if (num >= 0): s = '0'
    else:          s = '1'
    return s + b

def main():
    asic_hits = ''
    asic_length_prev = 0
    bxid_prev = '-0xf'
    asic_header_prev = ''
    strip_prev_list = []
    adc_prev_list = []
    # Read TELL40 Input
    data = open(filename,'r').read().split('\n')
    # SpillOver corrected Output
    ofile = open(newfilename, 'w')
    for line in data:
        if (line == ''):           ofile.write(line);      continue
        if ('Incomplete' in line): ofile.write(line+'\n'); continue
        # Nine standard columns. Tenth has commas.
        items = line.split(',',9)
        if (items[2] != 'Normal'): ofile.write(line+'\n'); continue
        if (items[8] == '0'):      ofile.write(line+'\n'); continue # Zero ASIC Length
        bxid = str(items[3])
        strip_list = []
        adc_list = []
        adc_raw_list = []
        asic_header = 'NP'
        asic_length = int(items[8])
        hits = items[9].split(',')
        for i in range (0,asic_length):
            s = hits[i]; pare = s[s.find("(")+1:s.find(")")]
            hinfo = s.replace('('+ pare +')', '')
            strip = pare.split(':')[0]
            adc   = pare.split(':')[1]
            strip_list.append(int(strip))
            adc_list.append(int(adc))
            adc_raw_list.append(int(adc))
        # SpillOver correction
        if (((int(bxid,16)-int(bxid_prev,16) == 1) or (int(bxid,16) == 0 and int(bxid_prev,16) == 3563)) and (asic_header == asic_header_prev)):
            for i in range (0,asic_length_prev):
                if (strip_list.count(strip_prev_list[i]) == 0):        continue
                j = strip_list.index(strip_prev_list[i])
                if (adc_prev_list[i] < 10):                            continue
                if (adc_prev_list[i] < 30):                            corr = 1
                if (adc_prev_list[i] == 30 or adc_prev_list[i] == 31): corr = 2
                if (corr < adc_list[j]): adc_list[j] = adc_list[j] - corr
                else:                    adc_list[j] = 0
        # ####################
        for i in range (0,asic_length):
            hinfo     = str(bin(strip_list[i])[2:].zfill(7)) + str(bin(adc_list[i])[2:].zfill(5))
            hinfo_hex = str(conv_binTohex(hinfo))
            asic_hits = asic_hits + hinfo_hex + '(' + str(strip_list[i]) + ':' + str(adc_list[i]) + '),'
        asic_hits = asic_hits[:-1]
        strNorm = items[0]+','+items[1]+','+items[2]+','+items[3]+','+items[4]+','+items[5]+','+items[6]+','
        strNorm = strNorm+hex(asic_length)[2:].upper()+','+str(asic_length)+','+asic_hits
        ofile.write(strNorm+'\n')
        asic_hits = ''
        bxid_prev = bxid
        asic_header_prev = asic_header
        asic_length_prev = asic_length
        strip_prev_list = strip_list
        adc_prev_list = adc_raw_list
    print('Done!')
   
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Correct humand-redable UT fiber input with SpillOver.')
    parser.add_argument('-i', '--inputfilename', dest='filename', default='Fiber_Decoded_Data/UTaX4a5_ASIC0_decoded.csv',
                        help='Human readable file for ASIC. Default is Fiber_Decoded_Data/UTaX4a5_ASIC0_decoded.csv')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/SpillOver/UTaX4a5_ASIC0_decoded.csv',
                        help='SpillOver corrected human readable file for ASIC. Default is Fiber_Decoded_Data/SpillOver/UTaX4a5_ASIC0_decoded.csv')
    args         = parser.parse_args()
    filename     = args.filename
    newfilename  = args.newfilename
    print(args)
    print ("Processing...")
    main()
