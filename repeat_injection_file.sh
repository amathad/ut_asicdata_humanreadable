#!/bin/bash

if (( $# != 2 )); then
    >&2 echo "This program creates a repeated humand-redable injection file."
    >&2 echo "Inputs: humand-redable UT fiber input; number of repeats." 
    >&2 echo "Usage:  $0 'STP_DataFiles_HW_Injection/FE_TELL40Input_4x3eports_HW_ASIC*_decoded.csv' 2"
    exit
fi

# Array of input files
array_fiber=($1)
num_repeat=$(($2))

for i in "${array_fiber[@]}"
do
    if [ -f "$i" ]; then
	new_file="${i::-4}_repeated.csv"
	>${new_file}
	tmpfile=$(mktemp /tmp/abc-script.XXXXXX)
	cat "$i" > "$new_file"
	lineNum="$(grep -n ",0x10a,266," $i | head -n 1 | cut -d: -f1)"
	sed -e "1,$(expr $lineNum - 1)d" "$i" > "$tmpfile"
	for j in $(seq 1 $num_repeat)
	do
	    cat "$tmpfile" >> "$new_file"
	done
    fi
done
