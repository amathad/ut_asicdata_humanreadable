#!/bin/python
import numpy as np
import pandas as pd
import argparse
import warnings
from math import ceil
from util import *
import os
import subprocess
import summarize_CSV

# Input: filename of the event list Questa file (!! NOT TABULAR LIST!!)
#        t0: moment when we start saving data
# Output: a dictionary of dictionaries.
#   example: mysignal['/top/tb/.../clk'][1234000]
#   will give you the value of signal /.../clk at time 1234000
def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split(" ",1)
            mysignal[name]={}
            mysignal[name][0]=value
            #print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split(" ",1)
                    if value_tmp.count("h") == 1:
                        [base, value] = value_tmp.split("h",1)
                    elif value_tmp.count("h") > 1:
                        value_tmp = value_tmp[1:-1]
                        [base, value] = value_tmp.split("h",1)
                        value = value.replace(base+"h","")
                    else:
                        value=value_tmp

                    mysignal[name][moment]=value
            line = myfile.readline().strip('\n')
    return mysignal

# Inputs:
#   thisSignal: a dictionary of {time: value}, for instance mySignal['/.../clk']
#   moment: an integer time, doesn't need to be in the dictionary
# Output:
#   The value of the signal at the specified time, extrapolated from the changes
# Example:
#   findSignalValue(mySignal['/.../clk'], 12345000) will give you the value of clk at that time

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

# Inputs:
#   sampledSignal: what signal to sample, [time, value]
#   clockSignal: what signal triggers the sampling (clk), [time, value]
#   enableSignal: what signal enables the sampling, [time, value]
#           -> 'none' disables this feature and will always sample
#
# Output:
#   a list of (time, value) of the sampled signal
#

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def associateBXID(BXID, Signal):
    BXID_cnt = 0
    associatedSignals = {}
    last_bxid = 0
    for s in Signal:
        timeStamp=s
        values=Signal[s]
        bxid=findSignalValue(BXID, timeStamp)  # hex
        #if bxid == 0xDEB:
        if int(bxid, base=16) < last_bxid:
            BXID_cnt += 1
        bxid_extended = str(BXID_cnt) + bxid
        if bxid_extended in associatedSignals:
            associatedSignals[bxid_extended]+= [values]
        else:
            associatedSignals[bxid_extended]=[values]
        
        last_bxid=int(bxid, base=16)
    return associatedSignals

def join_HL(H, L):
    tmp={}
    for (h, l) in zip(H, L):
        tmp[h]=H[h]+'_'+L[l]
    return tmp

def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_ST_sink_data_processing_name  = '/top/tb/TELL40/data_proc/InputBlock_'+lane+'/data_ST_sink_data_processing'
    valid_ST_sink_data_processing_name = '/top/tb/TELL40/data_proc/InputBlock_'+lane+'/valid_ST_sink_data_processing'
    input_data_processing_clock_name   = '/top/tb/TELL40/data_proc/InputBlock_'+lane+'/input_data_processing_clock'
    
    mySignals = read_questa(qevtfilename, int(dtime))

    data_ST_sink_tfc_processing = sampleSignal(mySignals['/top/tb/TELL40/data_proc/InputBlock_'+lane+'/data_ST_sink_tfc_processing'],
                                               mySignals[input_data_processing_clock_name], mySignals['/top/tb/TELL40/data_proc/InputBlock_'+lane+'/valid_ST_sink_tfc_processing'])
    BXID = slice_signal(data_ST_sink_tfc_processing, 64, 64-12, 12)
    first_key = list(BXID.keys())[0]
    first_BXID = BXID[first_key] # This is the first BXID we read from data_ST_sink_tfc_processing
    first_BXID = int(first_BXID,16)
    
    valid_ST_sink_data_processing = sampleSignal(mySignals[valid_ST_sink_data_processing_name], mySignals[input_data_processing_clock_name], 'none')
    for key in valid_ST_sink_data_processing.keys(): # Conversion from hex to binary
        if valid_ST_sink_data_processing[key] in '0123456789ABCDEF':
            if (nASICs == 2): aux = "{0:02b}".format(int(valid_ST_sink_data_processing[key],16))
            if (nASICs == 4): aux = "{0:04b}".format(int(valid_ST_sink_data_processing[key],16))
            valid_ST_sink_data_processing[key] = aux
            
    for i in range(0,nASICs):
        nASIC = (nASICs - 1) - i # ASIC numbers are reversed
        outputfile = newfilename+'_ASIC'+str(i)
        ofile = open(outputfile+'.txt', 'w')
        cont = 0
        for key in mySignals[data_ST_sink_data_processing_name].keys():
            cont = cont + 1
            if cont != 1: # format cleaning
                aux = mySignals[data_ST_sink_data_processing_name][key]
                values = aux.split(" ")
                data = values[nASIC]
                outstring = str(bin(int(data,16))[2:].zfill(bitwidth))+str(valid_ST_sink_data_processing[key][nASIC])+'\n'
                ofile.write(outstring)
        ofile.close()
        bashstring = 'python '+dir_path+'/conv_TELL40Output_To_HumanReadable.py -i '+outputfile+'.txt -o '+outputfile+'_decoded.csv -s '+scenario+' -b '+str(first_BXID)
        #os.system(bashstring)
        subprocess.run(bashstring.split())

    summarize_CSV.main(filename=newfilename+'_ASIC{0-'+str(nASICs-1)+'}_decoded.csv', newfilename=newfilename+'_decoded_summary.csv', scenario=scenario, init_bxid=first_BXID)

    synch_strings = []
    for i in range (1,11):
        bxid_dec = first_BXID-i
        bxid_hex = hex(bxid_dec)
        synch_strings.append("1,3,Sync,"+str(bxid_hex)+","+str(bxid_dec)+",111111111111")

    for i in range(0,nASICs):
        with open(newfilename+'_ASIC'+str(i)+'_decoded.csv', 'r+') as fp:
            lines = fp.readlines()
            for j in range (10): lines.insert(1, synch_strings[j]+"\n")
            fp.seek(0)
            fp.writelines(lines)
        fp.close()

    bashstring = 'python '+dir_path+'/OccupancyAndRare.py -i '+newfilename+'_ASIC*_decoded.csv -o '+newfilename+'_ASIC*_modified.csv -s '+scenario+' -m 0 -p 0 -f 0'
    subprocess.run(bashstring.split())
    bashstring = 'python '+dir_path+'/conv_HumanReadable_To_TELL40Input.py -i '+newfilename+'_ASIC{0-'+str(nASICs-1)+'}_modified.csv -o '+newfilename+'_TELL40Input.txt -s '+scenario
    subprocess.run(bashstring.split())

    df_output = pd.read_csv(newfilename+'_decoded_summary.csv')
    # Remove last column
    df_output = df_output.iloc[:,:-1]
    print ("TELL40 Output:")
    print (df_output.to_string())
    print('Done!')
   
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create a UT TELL40 input file from UT TELL40 InputBlock data. The numbering of ASICs in the summary file goes from right to left e.g. ASIC3/ASIC2/ASIC1/ASIC0.')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5',
                        help='Output files. Default is Fiber_Decoded_Data/UTaX4a5. Do not use a file extension. Binar files will be created as <newfilename>_ASIC*.txt. Human-readable files will be created as <newfilename>_ASIC*_decoded.csv and <newfilename>_ASIC*_modified.csv. A summary file for ASICs will be created as <newfilename>_decoded_summary.csv. A TELL40 input file will be created as <newfilename>_TELL40Input.txt')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports (obsolete), 2x4eports, 2x5eports. The 2x3eports case has 4 ASICs per Lane: Use 4x3eports option.')
    parser.add_argument('-q', '--questafilename', dest='qevtfilename', default='Questa_Data/list_UTaX4a5.lst',
                        help='Filename of the event list Questa file. Default is Questa_Data/list_UTaX4a5.lst')
    parser.add_argument('-l', '--lane', dest='lane', default='0',
                        help='Lane of interest for the event list Questa file. Default is 0')
    parser.add_argument('-t', '--dtime', dest='dtime', default='119380000',
                        help='Specify the time we start saving data in ps. Default is 119380000')
    args         = parser.parse_args()
    newfilename  = args.newfilename
    scenario     = args.scenario
    qevtfilename = args.qevtfilename 
    lane         = args.lane
    dtime        = args.dtime
    print(args)
    if (scenario == '4x3eports'): nASICs = 4; bitwidth = 24
    if (scenario == '2x3eports'): nASICs = 2; bitwidth = 24
    if (scenario == '2x4eports'): nASICs = 2; bitwidth = 32
    if (scenario == '2x5eports'): nASICs = 2; bitwidth = 40
    print ("Processing Questa file...")
    main()
