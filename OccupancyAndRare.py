#!/bin/python
import pandas as pd
import argparse
import warnings
import random
import math
import numpy as np
from util import *

def NormalPacket(currentline, filestreamtwo, nbits):
    global nHits_NP
    global nEvts_NP
    strNorm = currentline[0]+','+currentline[1]+','+currentline[2]+','+currentline[3]+',' \
             +currentline[4]+','+currentline[5]+','+currentline[6]+','
    currentline_join = ','.join(currentline)
    if (mean > 0): rhit = rng.binomial(n,p)+nHitsMin;
    else:          rhit = int(currentline[8])     
    strNorm = strNorm+hex(rhit)[2:].upper()+','+str(rhit)+','
    if (rhit == 0): strNorm = strNorm+'NoHits'; nbits += 12
    if (rhit > 63): strNorm = currentline[0]+','+currentline[1]+',BusyEvent,'+currentline[3]+','+currentline[4] \
                             +',0,1,0x13,19,NoData'; nbits += 12
    if (rhit > 0 and rhit < 64):
        li = range(0,128) # 7bits
        # We use this list to get non-repeating elemets
        rstrip_list = random.sample(li,rhit)
        rstrip_list.sort()
        for i in range (0,rhit):
            radcva = random.randint(1,31) # 5bits
            r12bit = hex(int(conv_numTobin(rstrip_list[i],'7b')+conv_numTobin(radcva,'5b'),2)) # 12bits
            rstrip = str(rstrip_list[i])
            radcva = str(radcva)
            strNorm = strNorm+r12bit+'('+rstrip+':'+radcva+'),'
        strNorm = strNorm[:-1]
        nbits += 12 + 12*rhit
    nHits_NP = nHits_NP + rhit
    nEvts_NP = nEvts_NP + 1
    if (mean > 0): filestreamtwo.write(strNorm+'\n')
    else:          filestreamtwo.write(currentline_join)
    return nbits

def RarePacket(currentline, filestreamtwo, nbits):
    global nHits_NP
    global nEvts_NP
    rrp = random.choice(ListRare)
    if (rrp==0): strRare = currentline[0]+','+currentline[1]+',BxVeto,'+currentline[3]+','+currentline[4] \
                          +',0,1,0x11,17,NoData'; nbits += 12
    if (rrp==1): strRare = currentline[0]+','+currentline[1]+',BufferFull,'+currentline[3]+','+currentline[4] \
                          +',0,1,0x14,20,NoData'; nbits += 12
    if (rrp==2): strRare = currentline[0]+','+currentline[1]+',BufferFullN,'+currentline[3]+','+currentline[4] \
                          +',0,1,0x15,21,NoData'; nbits += 12
    if (rrp==3): strRare = currentline[0]+','+currentline[1]+',BusyEvent,'+currentline[3]+','+currentline[4] \
                          +',0,1,0x13,19,NoData'; nbits += 12
    if (rrp==4): 
        strRare = currentline[0]+','+currentline[1]+',NZS,'+currentline[3]+','+currentline[4] \
                 +',0,1,0x6,6,'+hex(random.randint(0,2.604693e+238)) # (24+128*6)bits
        nbits += 12 + 24 + 6*128 # header + monitoring + channels
    currentline_join = ','.join(currentline)
    if (rrp in [0,1,2,3,4]): filestreamtwo.write(strRare+'\n')
    else:
        nHits_NP = nHits_NP + int(currentline[8])
        nEvts_NP = nEvts_NP + 1
        nbits += 12 + 12*int(currentline[8])
        filestreamtwo.write(currentline_join)
    return nbits

def NZSPacket(currentline, filestreamtwo, nbits):
    strNZS = currentline[0]+','+currentline[1]+',NZS,'+currentline[3]+','+currentline[4] \
            +',0,1,0x6,6,'+hex(random.randint(0,2.604693e+238)) # (24+128*6)bits
    nbits += 12 + 24 + 6*128 # header + monitoring + channels                     
    filestreamtwo.write(strNZS+'\n')
    return nbits

def FNZSPacket(currentline, filestreamtwo, nbits):
    strFNZS = currentline[0]+','+currentline[1]+',BufferFullN,'+currentline[3]+','+currentline[4] \
             +',0,1,0x15,21,NoData'
    nbits += 12
    filestreamtwo.write(strFNZS+'\n')
    return nbits

# Ax ≡ B (MOD C)
# A=12bits (Idle), B=-nbits,
# C=bitwidth (24, 32, 40 bits)
def congru(a,b,c):
    for i in range(0,c):
        if ((a*i - b)%c) == 0: fres = i; break
    return fres

def main():
    nbits_list = []
    for i in range (0,nASICS):
        nbits = 0
        inputfile  = inputfiles.replace("*", str(i))
        outputfile = outputfiles.replace("*", str(i))
        with open(inputfile, "r") as filestream:
            with open(outputfile, "w") as filestreamtwo:
                for line in filestream:
                    currentline = line.split(",")
                    if (currentline[2] == "Sync"): nbits += 24; filestreamtwo.write(line)
                    if (currentline[2] == "HeaderOnly"): nbits += 12; filestreamtwo.write(line)
                    if (currentline[2] == "Idle\n"): nbits += 12; filestreamtwo.write(line);
                    if (currentline[2] == "BxVeto"): nbits += 12; filestreamtwo.write(line);
                    if (currentline[2] == "BufferFull"): nbits += 12; filestreamtwo.write(line);
                    if (currentline[2] == "BufferFullN"): nbits += 12; filestreamtwo.write(line);
                    if (currentline[2] == "NZS"): 
                        rfnzs = random.uniform(0,1)
                        if (rfnzs>=ProbFNZS): nbits = NZSPacket(currentline, filestreamtwo, nbits)
                        else:                 nbits = FNZSPacket(currentline, filestreamtwo, nbits)         
                    if (currentline[2] == "BusyEvent"): nbits += 12; filestreamtwo.write(line);
                    if (currentline[2] == "Normal"):
                        rrare = random.uniform(0,1)
                        if (rrare>=ProbRare): nbits = NormalPacket(currentline, filestreamtwo, nbits)
                        else:                 nbits = RarePacket(currentline, filestreamtwo, nbits)
                    if (currentline[2] == "PacketName"): filestreamtwo.write(line)
        nbits_list.append(nbits)
    print ("Total number of bits for each ASIC:", nbits_list)
    maxbits = max(nbits_list) + 12*congru(12,-max(nbits_list),bitwidth)
    print ("All files will have",maxbits,"bits by adding Idle packets.")
    for i in range (0,nASICS):
        outputfile = outputfiles.replace("*", str(i))
        file_object = open(outputfile, 'a')
        for j in range (0,int((maxbits-nbits_list[i])/12)):
            file_object.write("100,3,Idle\n")
        file_object.close()
    print ("Mean occupancy of {} normal packets:".format(nEvts_NP), round(nHits_NP/nEvts_NP,5), "hits")
    print('Done!')
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Change the occupancy and add rare events to humand-redable UT fiber input files.')
    parser.add_argument('-i', '--inputfilename', dest='inputfiles', default='Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_decoded.csv',
                        help='Human readable files for ASICs. Denote the ASICID with an asterisk in the name'
                             ' e.g. python OccupancyAndRare.py -i "Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_decoded.csv".')
    parser.add_argument('-o', '--outputfilename', dest='outputfiles', default='Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_modified.csv',
                        help='Output file. Default is "Fiber_Decoded_Data/4x3eports/UTaX1b2_ASIC*_modified.csv". Use an asterisk to denote the different ASICs.')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    parser.add_argument('-n', '--nHitsMin', dest='nHitsMin', default='0',
                        help='Change the occupancy of each ASIC based on a binomial distribution from nHitsMin to nHitsMax. Default nHitsMin is 0 hits.')
    parser.add_argument('-x', '--nHitsMax', dest='nHitsMax', default='100',
                        help='Change the occupancy of each ASIC based on a binomial distribution from nHitsMin to nHitsMax. Default nHitsMax is 100 hits.')
    parser.add_argument('-m', '--mean', dest='mean', default='1.0',
                        help='Mean of the binomial distribution (mean occupancy). A zero or negative value will not change the occupancy. Default is 1.0 hits.')
    parser.add_argument('-p', '--ProbRare', dest='ProbRare', default='0.05',
                        help='The probability to have a rare event instead of a normal packet. Default is 0.05, 5 per cent.')
    parser.add_argument('-l', '--ListRare', dest='ListRare', default='01234',
                        help='String list of rare events to be added. 0:BxVeto, 1:BufferFull, 2:BufferFullN, 3:BusyEvent, 4:NZS. Default is 01234.')
    parser.add_argument('-f', '--ProbFNZS', dest='ProbFNZS', default='0.05',
                        help='The probability to have a BufferFullN event instead of an NZS packet. Default is 0.05, 5 per cent.')
    args         = parser.parse_args()
    inputfiles   = args.inputfiles
    outputfiles  = args.outputfiles
    scenario     = args.scenario
    nHitsMin     = int(args.nHitsMin)
    nHitsMax     = int(args.nHitsMax)
    mean         = float(args.mean)
    ProbRare     = float(args.ProbRare)
    ProbFNZS     = float(args.ProbFNZS)
    ListRare     = list(map(int, list(args.ListRare)))
    print(args)
    if (scenario == '4x3eports'): nASICS = 4; bitwidth = 24
    if (scenario == '2x3eports'): nASICS = 2; bitwidth = 24
    if (scenario == '2x4eports'): nASICS = 2; bitwidth = 32
    if (scenario == '2x5eports'): nASICS = 2; bitwidth = 40
    rng = np.random.default_rng()
    # Binomial distribution
    if (mean > 0):
        n = (nHitsMax-nHitsMin) # number of trials
        p = (mean-nHitsMin)/n # probability of each trial
        print ("Binomial distribution: n = ", round(n,5), ", p = ", round(p,5), sep='')
    nHits_NP = 0
    nEvts_NP = 0
    main()
