#!/bin/python
import pandas as pd
import argparse
import warnings
import re

def main():
    gbtcomp = re.split('(\d+)',gbtname)
    if (gbtcomp[2]=='a'): gbtcomp[2]='alpha'
    if (gbtcomp[2]=='b'): gbtcomp[2]='beta'
    if (gbtcomp[2]=='g'): gbtcomp[2]='gamma'
    print ("DCB index:", gbtcomp[1])
    print ("BP variant:", gbtcomp[2])
    print ("GBTx index:", gbtcomp[3])
    df = pd.read_csv("AsicToFiberMapping.csv")
    #df = df.iloc[: , :-6]
    df.rename(columns = {'BP variant (alpha/beta/gamma)':'BP variant',
                         'GBTx channels (GBT frame bytes)':'GBTx channels'}, inplace = True)
    dfa = df[(df["Stave"].str[:4]==gbtcomp[0])  &  (df["DCB index"]==int(gbtcomp[1])) & (df["BP variant"]==gbtcomp[2]) & (df["GBTx index"]==int(gbtcomp[3]))]
    dfa = dfa.reset_index(drop=True);
    
    print ("\nFiber to ASICs mapping:")
    print (dfa[["PEPI", "Stave", "Flex", "Hybrid", "GBTx channels", "ASIC index"]])
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fiber to ASICs mapping based on AsicToFiberMapping.csv. \
                                                  The naming scheme of GBTs is layer (e.g. UTaX), then DCB number, backplane letter (a for alpha, b for beta, g for gamma) and GBT number.')
    parser.add_argument('-n', '--gbtname', dest='gbtname', default='UTaX0b5', help='The name of the GBT. Default is UTaX0b5.')
    args     = parser.parse_args()
    gbtname  = args.gbtname
    print(args)
    main()
