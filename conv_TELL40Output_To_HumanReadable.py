#!/bin/python
import numpy as np
import argparse
from util import *

def FileToASICLines(Scenario, Filename):
    linnum_list, zerosbefore_list, data_list = [], [], []
    with open(Filename, 'r') as myfile:
        line = myfile.readline()
        line = line.strip('\n')
        count= 0
        bits = ''
        linenum  = 0
        zerosnum = 0
        while(line != ''):
            #if the first line ends with  one
            if line[-1] == '1':
                bits += line[:-1]
                line  = myfile.readline()
                line  = line.strip('\n')
                linenum+= 1
                continue

            #if the first line is zero
            if bits == '':
                line    = myfile.readline()
                line    = line.strip('\n')
                linenum += 1
                zerosnum+= 1
                continue

            #run the following when the next line is zero (NB: when the last line is zero this never runs go to else statement)
            #if len(bits)%24!=0: raise Exception('The BXID is not a multple of 24b. Check!')

            #print(linenum, zerosnum, bits)
            linnum_list     += [linenum]
            zerosbefore_list += [zerosnum]
            data_list       += [bits]
            linenum  += 1
            zerosnum += 1
            bits      = ''
            line      = myfile.readline()
            line      = line.strip('\n')
        else:
            #if there is one on the last line
            if bits != '':
                #print(linenum, zerosnum, bits)
                linnum_list     += [linenum]
                zerosbefore_list += [zerosnum]
                data_list       += [bits]

    zerosafter_list = list(np.array(zerosbefore_list[1:]) - np.array(zerosbefore_list[:-1])) + [zerosnum - zerosbefore_list[-1]]
    return linnum_list, zerosafter_list, data_list

def Identify_Datapacket(chunkbits, linenum, zerosafter, bit_divs, prev_datapackets = None):
    #first check for the sync data pattern: fill the readout
    readout   = None
    prev_bxid = None
    if prev_datapackets is not None:
        prev_datap = prev_datapackets[-1]
        prev_bxid  = prev_datap.bxid
    else:
        if init_bxid is not None: 
            prev_bxid  = conv_numTobin(int(init_bxid), '12b')
        else:
            print("WARNING!!: DO NOT START THE FILE WITH A SYNC")


    sync_bxid = chunkbits[:12] #get the first 12b for sync data
    sync_patt = chunkbits[12:12+12] #move to next 12b to extract sync_patt
    if prev_bxid is not None:
        if conv_binTonum(prev_bxid)+1 == conv_binTonum(sync_bxid): #conduct checks to identify sync pattern
            if conv_hexTobin('0xcaa', bitwidth = '12b') == sync_patt: #conduct checks to identify sync pattern
                #print('This is a sync packet')
                readout  = SyncPacket('Sync', sync_bxid)
        #else:
        #    raise Exception('The previous bxid is', conv_binTonum(prev_bxid), 'which is not sequential to the current bxid which is', conv_binTonum(sync_bxid))

    #if the readout is None (i.e. no sync pattern filled) then fill other packets (except idle) and update pos_toreadfrom
    if readout is None:
        #split into bxid, parity, flag, length
        splitbits = [chunkbits[bit_divs[i]:bit_divs[i+1]] for i in range(len(bit_divs)) if i+1 != len(bit_divs)]
        if splitbits[2] == '1': #If flag is 1
            packetname = None
            if splitbits[3] == '010001': 
                packetname = 'BxVeto'
            elif splitbits[3] == '010010':
                packetname = 'HeaderOnly'
            elif splitbits[3] == '010011':
                packetname = 'BusyEvent'
            elif splitbits[3] == '010100':
                packetname = 'BufferFull'
            elif splitbits[3] == '010101':
                packetname = 'BufferFullN'
            elif splitbits[3] == '000110':
                packetname = 'NZS'
            else:
                raise Exception('With the flag of 1, none of the data packets match. Please check!')

            readout = DataPacket(packetname, splitbits[0][-4:], splitbits[1], splitbits[2], splitbits[3])
            #fill NZS data
            if packetname == 'NZS':
                pos_toreadfrom  = 20
                pos_toreadtill  = pos_toreadfrom + 24 + 128*6
                nzsdata         = chunkbits[pos_toreadfrom:pos_toreadtill]
                if len(nzsdata) == 24 + 128*6:
                    readout.data = conv_binTohex(nzsdata)
                else:
                    print('The NZS data packet is not 712b long. Check if your input file has full packet info!')
            else:
                readout.data   = 'NoData'
        elif splitbits[2] == '0': #If flag is zero
            readout         = DataPacket('Normal', splitbits[0][-4:], splitbits[1], splitbits[2], splitbits[3])
            #get the normal packet hitdata
            nhits           = conv_binTonum(splitbits[-1])
            pos_toreadfrom  = 20
            pos_toreadtill  = pos_toreadfrom + nhits*12
            hitdata         = chunkbits[pos_toreadfrom:pos_toreadtill]
            if len(hitdata) == nhits*12:
                splithitdata = [conv_binTohex(hitdata[i*12:i*12+12]) for i in range(nhits)]
                if len(splithitdata) == 0:
                    readout.data = 'NoHits'
                else:
                    readout.data = splithitdata
            else:
                readout.data = 'NoData'
                print('Caution! Some of the hit data overflows and is not contained in the input file. Check if your input file has full hit info!')
        else:
            raise Exception('Flag is not recognished please check!')

    readout.zerosafter = zerosafter
    readout.linenum    = linenum
    return readout

def ASICLineSplit(linnums, zerosafters, databits):
    #define length of header
    bit_divs     = np.cumsum([0,12,1,1,6]) #bxid, parity, flag, nhits
    if bit_divs[-1] != 20: raise Exception('ASIC header does not add to 20b. Please check!') 

    readouts        = []
    count           = 0
    for linnum, zerosafter, databit in zip(linnums, zerosafters, databits):
        if len(readouts) == 0:
            readout = Identify_Datapacket(databit, linnum, zerosafter, bit_divs)
        else:
            readout = Identify_Datapacket(databit, linnum, zerosafter, bit_divs, prev_datapackets = readouts)

        if readout.name != 'Sync':
            print(readout.linenum, readout.name, readout.bxid+' ('+str(conv_binTonum(readout.bxid))+')', conv_binTonum(readout.parity), conv_binTonum(readout.flag), \
                  readout.length+' ('+str(conv_binTonum(readout.length))+')', readout.data)
        else:
            print(readout.linenum, readout.name, readout.bxid+' ('+str(conv_binTonum(readout.bxid))+')', readout.data)

        readouts += [readout]
        count += 1
        #if count == 200: break

    return readouts

def write_csv(Newfilename, asicheaders, readouts):
    print('Writing a new file', Newfilename)
    with open(Newfilename, 'w') as csvfile: 
        #print(asicheaders)
        csvfile.write(asicheaders)
        for readout in readouts:
            name       = readout.name
            linenum    = readout.linenum
            zerosafter = readout.zerosafter
            if name == 'Sync':
                bxidhex   = conv_binTohex(readout.bxid)
                bxiddec   = conv_binTonum(readout.bxid)
                HitDataHex= readout.data
                s = '{0:},{1:},{2:},{3:},{4:}\n'.format(linenum,zerosafter,name,bxidhex,bxiddec)
            elif name == 'Normal':
                bxidhex   = conv_binTohex(readout.bxid)
                bxiddec   = conv_binTonum(readout.bxid)
                parity    = conv_binTonum(readout.parity)
                flag      = conv_binTonum(readout.flag)
                lengthHex = conv_binTohex(readout.length)
                lengthdec = conv_binTonum(readout.length)
                HitDataHex= ''
                if not isinstance(readout.data, list):
                    HitDataHex = readout.data
                else:
                    for i in range(len(readout.data)):
                        d       = readout.data[i]
                        databin = conv_hexTobin(d, bitwidth = '12b')
                        sid = conv_binTonum(databin[:7])
                        adc = conv_binTonum(databin[7:12])
                        if i != len(readout.data)-1:
                            HitDataHex  += d+'('+str(sid)+':'+str(adc)+'),'
                        else:
                            HitDataHex  += d+'('+str(sid)+':'+str(adc)+')'

                s = '{0:},{1:},{2:},{3:},{4:},{5:},{6:},{7:},{8:},{9:}\n'.format(linenum,zerosafter,name,bxidhex,bxiddec,parity,flag,lengthHex,lengthdec,HitDataHex)
            elif name == 'NZS':
                bxidhex   = conv_binTohex(readout.bxid)
                bxiddec   = conv_binTonum(readout.bxid)
                parity    = conv_binTonum(readout.parity)
                flag      = conv_binTonum(readout.flag)
                lengthHex = conv_binTohex(readout.length)
                lengthdec = conv_binTonum(readout.length)
                HitDataHex= readout.data
                s = '{0:},{1:},{2:},{3:},{4:},{5:},{6:},{7:},{8:},{9:}\n'.format(linenum,zerosafter,name,bxidhex,bxiddec,parity,flag,lengthHex,lengthdec,HitDataHex)
            else:
                bxidhex   = conv_binTohex(readout.bxid)
                bxiddec   = conv_binTonum(readout.bxid)
                s = '{0:},{1:},{2:},{3:},{4:}\n'.format(linenum,zerosafter,name,bxidhex,bxiddec)

            #print(s)
            csvfile.write(s)
    
def main():
    #get the asic data
    linnums, zerosafters, databits = FileToASICLines(scenario, filename)
    #print(linnums, zerosafters, databits)

    #write csv files after getting readouts
    asicheaders = '{0:},{1:},{2:},{3:},{4:},{5:},{6:},{7:},{8:},{9:}\n'.format('LineNum','ZerosAfter','PacketName','BXIDHex',\
                  'BXIDDecimal','Parity','Flag','LengthHex','LengthDecimal','HitDataHex(StripID:ADCVal)')

    readouts    = ASICLineSplit(linnums, zerosafters, databits)
    write_csv(newfilename, asicheaders, readouts)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert UT fiber output to human-redable format.')
    parser.add_argument('-i'  , '--inputfilename' , dest='filename'   , default='ASIC0.txt', help='Binary file ouput (include absolute or relative path) of TELL40 board. Default is ASIC0.txt.')
    parser.add_argument('-o'  , '--outputfilename', dest='newfilename', default='ASIC_Decoded_Data/ASIC0_decoded.csv', help='Human readable file. Default is ASIC_Decoded_Data/ASIC0_decoded.csv')
    parser.add_argument('-s'  , '--scenario'      , dest='scenario'   , default='4x3eports'  , help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports, 2x4eports, 2x5eports')
    parser.add_argument('-b'  , '--init_bxid'     , dest='init_bxid'  , default=None, help='What is the initial bunch crossing ID, needed if first packet is a sync. Default is None and warns user about first packet cannot be a sync')
    args        = parser.parse_args()
    filename    = args.filename
    newfilename = args.newfilename
    scenario    = args.scenario
    init_bxid   = args.init_bxid
    print(args)
    main()
