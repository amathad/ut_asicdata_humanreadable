# UT TELL40 input conversion to human-readable format

Code in this project is used for debugging the UT firmware implementation.

## Description of files

All the files have an optional argument for help (-h, --help).

**conv_TELL40Input_To_HumanReadable.py**: Convert UT fiber input to human-redable format.

**conv_HumanReadable_To_TELL40Input.py**: Convert humand-redable UT fiber input to binary format.

**conv_TELL40Output_To_HumanReadable.py**: Convert UT fiber output to human-redable format.

**conv_HumanReadable_To_TELL40Output.py**: Convert humand-redable UT fiber output to binary format.

**summarize_CSV.py**: Summary of humand-redable UT fiber input.

**ReadQuestaData.py**: Summary of humand-redable UT fiber input and read Questa data.

**OccupancyAndRare.py**: Change the occupancy and add rare events to humand-redable UT fiber input files.

**FindSensorGBT.py**: ASICs to fiber mapping for a specific sensor based on AsicToFiberMapping.csv.

**FindNameGBT.py**: Fiber to ASICs mapping based on AsicToFiberMapping.csv.

**RemoveIncompleteData.py**: Remove incomplete data from humand-redable UT fiber input files. 

**ReadTELL40Output.py**: Summary of humand-redable UT TELL40 output. 

**ReadOutputBlock.py**: Summary of humand-redable UT TELL40 OutputBlock. 

**conv_STP_To_QuestaEvtList.py**: Convert STP CSV file to Questa event list.

**corr_SpillOver_To_HumanReadable.py**: Correct humand-redable UT fiber input with SpillOver.

**ReadSpillOver.py**: Summary of SpillOver UT TELL40 correction.

**comb_STP_To_QuestaEvtList.py**: Combine STP Questa event lists to a single list.

**conv_InputBlock_To_TELL40Input.py**: Create a UT TELL40 input file from UT TELL40 InputBlock data.

## How to add all packets to human-readable files

```
python OccupancyAndRare.py -i "test/FE_0_TELL40Input_ASIC*_decoded.csv" -o "test/FE_0_TELL40Input_ASIC*_modified0.csv" -s 4x3eports -n 0 -x 50 -m 0 -p 0.10 -l 01234 -f 0
cp test/FE_0_TELL40Input_ASIC0_modified0.csv test/FE_0_TELL40Input_ASIC1_modified0.csv
cp test/FE_0_TELL40Input_ASIC0_modified0.csv test/FE_0_TELL40Input_ASIC2_modified0.csv
cp test/FE_0_TELL40Input_ASIC0_modified0.csv test/FE_0_TELL40Input_ASIC3_modified0.csv
python OccupancyAndRare.py -i "test/FE_0_TELL40Input_ASIC*_modified0.csv" -o "test/FE_0_TELL40Input_ASIC*_modified1.csv" -s 4x3eports -n 0 -x 50 -m 0.19224 -p 0.10 -l 13 -f 0.10
```