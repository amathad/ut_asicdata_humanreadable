#!/bin/python
import numpy as np
import pandas as pd
import argparse
import warnings
from math import ceil
from util import *
import os
import subprocess
import summarize_CSV

# Input: filename of the event list Questa file (!! NOT TABULAR LIST!!)
#        t0: moment when we start saving data
# Output: a dictionary of dictionaries.
#   example: mysignal['/top/tb/.../clk'][1234000]
#   will give you the value of signal /.../clk at time 1234000
def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split(" ",1)
            mysignal[name]={}
            mysignal[name][0]=value
            #print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split(" ",1)
                    if value_tmp.count("h") == 1:
                        [base, value] = value_tmp.split("h",1)
                    elif value_tmp.count("h") > 1:
                        value_tmp = value_tmp[1:-1]
                        [base, value] = value_tmp.split("h",1)
                        value = value.replace(base+"h","")
                    else:
                        value=value_tmp

                    mysignal[name][moment]=value
            line = myfile.readline().strip('\n')
    return mysignal

# Inputs:
#   thisSignal: a dictionary of {time: value}, for instance mySignal['/.../clk']
#   moment: an integer time, doesn't need to be in the dictionary
# Output:
#   The value of the signal at the specified time, extrapolated from the changes
# Example:
#   findSignalValue(mySignal['/.../clk'], 12345000) will give you the value of clk at that time

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

# Inputs:
#   sampledSignal: what signal to sample, [time, value]
#   clockSignal: what signal triggers the sampling (clk), [time, value]
#   enableSignal: what signal enables the sampling, [time, value]
#           -> 'none' disables this feature and will always sample
#
# Output:
#   a list of (time, value) of the sampled signal
#

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def associateBXID(BXID, Signal):
    BXID_cnt = 0
    associatedSignals = {}
    last_bxid = 0
    for s in Signal:
        timeStamp=s
        values=Signal[s]
        bxid=findSignalValue(BXID, timeStamp)  # hex
        #if bxid == 0xDEB:
        if int(bxid, base=16) < last_bxid:
            BXID_cnt += 1
        bxid_extended = str(BXID_cnt) + bxid
        if bxid_extended in associatedSignals:
            associatedSignals[bxid_extended]+= [values]
        else:
            associatedSignals[bxid_extended]=[values]
        
        last_bxid=int(bxid, base=16)
    return associatedSignals

def join_HL(H, L):
    tmp={}
    for (h, l) in zip(H, L):
        tmp[h]=H[h]+'_'+L[l]
    return tmp

def main():
    SO_DataOut_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/SO_DataOut'
    SO_DataInRdReq_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/SO_DataInRdReq'
    BXID_SpillOver_SO_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/BXID_SpillOver_SO'
    HEAD_SpillOver_SO_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/HEAD_SpillOver_SO'
    DP_clock_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/DP_clock'

    mySignals = read_questa(qevtfilename, int(dtime))

    # SpillOver trigger
    # /top/tb/TELL40/data_proc/SpillOver_0/IB_EventFIFO_Out(83)
    # readout40-firmware/ut/packages/4x3eports/detector_constant_declaration_sim.vhd (CALIBRATION TRIGGERS)
    BXID_SpillOver_IB_name  = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/BXID_SpillOver_IB'
    TRIG_SpillOver_IB_name  = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/TRIG_SpillOver_IB'
    IB_EventFIFO_rdreq_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/IB_EventFIFO_rdreq'
    DP_clock_name = '/top/tb/TELL40/data_proc/SpillOver_'+lane+'/DP_clock'
    BXID_SpillOver_IB = sampleSignal(mySignals[BXID_SpillOver_IB_name], mySignals[DP_clock_name], mySignals[IB_EventFIFO_rdreq_name])
    TRIG_SpillOver_IB = sampleSignal(mySignals[TRIG_SpillOver_IB_name], mySignals[DP_clock_name], mySignals[IB_EventFIFO_rdreq_name])
    cycle = 0
    BXID_prev = '-0xf'
    TRIG_SpillOver_IB_list = []
    for key in BXID_SpillOver_IB.keys():
        if (int(BXID_SpillOver_IB[key],16) < int(BXID_prev,16)): cycle = cycle + 1
        if TRIG_SpillOver_IB[key] in '0123456789ABCDEF':
            aux = "{0:01b}".format(int(TRIG_SpillOver_IB[key],16))
            TRIG_SpillOver_IB[key] = aux
            if (aux == '0'): TRIG_SpillOver_IB_list.append(str(cycle)+":"+'0x'+hex(int(BXID_SpillOver_IB[key],16))[2:].zfill(3))
        BXID_prev = BXID_SpillOver_IB[key]

    # DATA
    SO_DataOut = sampleSignal(mySignals[SO_DataOut_name], mySignals[DP_clock_name], 'none')
    SO_DataInRdReq = sampleSignal(mySignals[SO_DataInRdReq_name], mySignals[DP_clock_name], 'none')
    BXID_SpillOver_SO = sampleSignal(mySignals[BXID_SpillOver_SO_name], mySignals[DP_clock_name], 'none')
    HEAD_SpillOver_SO = sampleSignal(mySignals[HEAD_SpillOver_SO_name], mySignals[DP_clock_name], 'none')

    for key in SO_DataInRdReq.keys(): # Conversion from hex to binary 
        if SO_DataInRdReq[key] in '0123456789ABCDEF':
            aux = "{0:04b}".format(int(SO_DataInRdReq[key],16))
            aux = aux[::-1]
            SO_DataInRdReq[key] = aux[int(ASIC)]

    ofile = open(newfilename, 'w')
    cont = 0
    for key in SO_DataOut.keys():
        cont = cont + 1
        if cont != 1: # format cleaning
            data_asic = SO_DataOut[key].split()
            data_asic.reverse()
            data = data_asic[int(ASIC)]
            if (str(SO_DataInRdReq[key]) == '1'): # write only valid
                aux = str(bin(int(HEAD_SpillOver_SO[key],16))[2:].zfill(32))
                aux_list = [aux[start:start+8] for start in range(0, len(aux), 8)]
                aux_list.reverse()
                HEAD_SpillOver_SO[key] = aux_list[int(ASIC)]
                outstring = str(bin(int(data,16))[2:].zfill(32))+str(SO_DataInRdReq[key])+ \
                            str(BXID_SpillOver_SO[key])+str(HEAD_SpillOver_SO[key])+'\n'
                ofile.write(outstring)
    ofile.close()

    # Read SpillOver data
    print ("Processing the events...")
    Output_names = ['Hit1','Hit0','valid','BXID','Header']
    Output_width = [16,16,1,3,8]
    df = pd.read_fwf(newfilename, widths=Output_width, names=Output_names, header=None, dtype=str)
    df['BXID_next'] = df['BXID'].shift(-1)
    df['BXID_prev'] = df['BXID'].shift(1)

    cycle = 0
    asic_hits = ''
    cycle_list = []
    bxid_list = []
    asic_header_list = []
    asic_length_list = []
    asic_hits_list = []
    for index, row in df.iterrows():
        if (index != 0 and int(row['BXID'],16) < int(row['BXID_prev'],16)): cycle = cycle + 1
        asic_hits = row['Hit0'][-12:] + asic_hits; asic_hits = row['Hit1'][-12:] + asic_hits
        if (row['BXID'] != row['BXID_next']):
            asic_header = row['Header']
            asic_length = int(asic_header[-6:],2)
            asic_hits = asic_hits[-12*asic_length:]
            if (asic_header[1] == '0'): asic_header = 'NP'
            else:                       asic_header = 'U'
            string_bxid = '0x' + hex(int(row['BXID'],16))[2:].zfill(3)
            cycle_list.append(str(cycle))
            bxid_list.append(str(string_bxid))
            asic_header_list.append(asic_header)
            asic_length_list.append(asic_length)
            asic_hits_list.append(str(conv_binTohex(asic_hits)))
            asic_hits = ''
    df_sover = pd.DataFrame(list(zip(cycle_list, bxid_list, asic_header_list, asic_length_list, asic_hits_list)),
                            columns = ['Cycle', 'BXIDHex', 'Packet', 'Length', 'Hits'])

    cycle = 0
    asic_hits = ''
    cycle_list = []
    bxid_list = []
    asic_header_list = []
    asic_length_list = []
    asic_hits_list = []
    # Read TELL40 Input
    data = open(filename,'r').read().split('\n')
    for line in data:
        if (line == ''):           continue
        if ('Incomplete' in line): continue
        # Nine standard columns. Tenth has commas.
        items = line.split(',',9)
        if (len(items) > 3 and items[3] == '0x000'): cycle = cycle + 1
        if (items[2] != 'Normal'): continue
        if (items[8] == '0'):      continue # Zero ASIC Length
        bxid = str(items[3])
        asic_header = 'NP'
        asic_length = int(items[8])
        hits = items[9].split(',')
        for i in range (0,asic_length):
            s = hits[i]; pare = s[s.find("(")+1:s.find(")")]
            strip = int(pare.split(':')[0])
            adc   = int(pare.split(':')[1])
            hinfo = str(bin(strip)[2:].zfill(7)) + str(bin(adc)[2:].zfill(5))
            asic_hits = hinfo + asic_hits
        cycle_list.append(str(cycle))
        bxid_list.append(bxid)
        asic_header_list.append(asic_header)
        asic_length_list.append(int(asic_length))
        asic_hits_list.append(str(conv_binTohex(asic_hits)))
        asic_hits = ''
    df_input = pd.DataFrame(list(zip(cycle_list, bxid_list, asic_header_list, asic_length_list, asic_hits_list)), 
                            columns = ['Cycle', 'BXIDHex', 'Packet', 'Length', 'Hits'])

    #index_last = len(list(df_sover.index.values))
    #df_input.drop(df_input.index[index_last:], inplace=True)

    bxid_ini = df_sover['BXIDHex'].iloc[0]
    bxid_fin = df_sover['BXIDHex'].iloc[-1]
    cycl_ini = df_sover['Cycle'].iloc[0]
    cycl_fin = df_sover['Cycle'].iloc[-1]
    index_first = df_input.loc[ (df_input['BXIDHex'] == bxid_ini) & (df_input['Cycle'] == cycl_ini) ].index[0]
    df_input.drop(df_input.index[0:index_first], inplace=True)
    df_input.reset_index(drop=True, inplace = True)
    index_last = df_input.loc[ (df_input['BXIDHex'] == bxid_fin) & (df_input['Cycle'] == cycl_fin) ].index[0]
    df_input.drop(df_input.index[index_last+1:], inplace=True)

    # Merge two dataFrames and add indicator column
    df_all = pd.merge(df_input, df_sover, on=['Cycle','BXIDHex','Packet','Length','Hits'], how='left', indicator='Exists')
    # Add column to show if each row in first DataFrame exists in second
    df_all['Exists'] = np.where(df_all.Exists == 'both', True, False)

    SpillOver_summary_list = []
    for index, row in df_all.iterrows():
        SpillOver_summary_temp = df_sover[ (df_sover['BXIDHex'] == row['BXIDHex']) & (df_sover['Cycle'] == row['Cycle'])].to_string(index=False, header=None)
        if (SpillOver_summary_temp[:15] == "Empty DataFrame"):
            if (str(row['Cycle'])+":"+str(row['BXIDHex']) in TRIG_SpillOver_IB_list): SpillOver_summary_temp = "SO Trigger"
            else:                                                                     SpillOver_summary_temp = "Empty BXID"
        SpillOver_summary_list.append(SpillOver_summary_temp)
    df_all["SpillOver"] = SpillOver_summary_list

    print ("TELL40 Input/SpillOver comparison:")
    print (df_all.to_string(max_colwidth=80))
    print ("TELL40 Input/SpillOver differences:")
    df_all = df_all[df_all['SpillOver'] != "SO Trigger"]
    print (df_all[df_all['Exists'] == False].to_string(max_colwidth=80))
    if (not df_all[df_all['Exists'] == False].empty): status_asic = 0
    if (    df_all[df_all['Exists'] == False].empty): status_asic = 1
    if (status_asic == 0): print ("Status: *Error*")
    if (status_asic == 1): print ("Status: All OK!")
    print('Done!')
   
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Summary of SpillOver UT TELL40 correction.')
    parser.add_argument('-i', '--inputfilename', dest='filename', default='Fiber_Decoded_Data/UTaX4a5_ASIC0_decoded.csv',
                        help='Human readable file for ASIC. Default is Fiber_Decoded_Data/UTaX4a5_ASIC0_decoded.csv')
    parser.add_argument('-o', '--outputfilename', dest='newfilename', default='Fiber_Decoded_Data/UTaX4a5.txt',
                        help='Output binar file. Default is Fiber_Decoded_Data/UTaX4a5.txt')
    parser.add_argument('-s', '--scenario', dest='scenario', default='4x3eports',
                        help='Which GBT frame byte is the input file in? Options are: 4x3eports (default), 2x3eports (obsolete), 2x4eports, 2x5eports. The 2x3eports case has 4 ASICs per Lane: Use 4x3eports option.')
    parser.add_argument('-q', '--questafilename', dest='qevtfilename', default='Questa_Data/list_UTaX4a5.lst',
                        help='Filename of the event list Questa file. Default is Questa_Data/list_UTaX4a5.lst')
    parser.add_argument('-l', '--lane', dest='lane', default='0',
                        help='Lane of interest for the event list Questa file. Default is 0')
    parser.add_argument('-a', '--ASIC', dest='ASIC', default='0',
                        help='ASIC of interest for the event list Questa file. Default is 0')
    parser.add_argument('-t', '--dtime', dest='dtime', default='119380000',
                        help='Specify the time we start saving data in ps. Default is 119380000')
    args         = parser.parse_args()
    filename     = args.filename
    newfilename  = args.newfilename
    scenario     = args.scenario
    qevtfilename = args.qevtfilename 
    lane         = args.lane
    ASIC         = args.ASIC
    dtime        = args.dtime
    print(args)
    if (scenario == '4x3eports'): nASICs = 4; bitwidth = 24
    if (scenario == '2x3eports'): nASICs = 2; bitwidth = 24
    if (scenario == '2x4eports'): nASICs = 2; bitwidth = 32
    if (scenario == '2x5eports'): nASICs = 2; bitwidth = 40
    print ("Processing Questa file...")
    main()
