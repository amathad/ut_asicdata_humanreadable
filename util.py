#!/bin/python

import re


class DataPacket():  
    def __init__(self, name, bxid, parity, flag, length, bxid_full = None):
        #All must be binary strings apart from name
        self.name   = name   
        self.bxid   = bxid   
        self.bxid_full = bxid_full
        self.flag   = flag   
        self.length = length 
        self.data   = None   #check data that it is multiple of 12b
        self.linenum= None
        self.zerosafter= None #dummy variable
        self.header    = self.bxid+self.flag+self.length
        self.parity = parity
        #parity check to expected
        ones = (self.header).count('1')
        expected_parity = '1'
        if ones%2 == 0: expected_parity = '0'
        if self.parity != expected_parity: 
            print("WARNING Expected parity",expected_parity,"does not match the given",self.parity)

class DataPacket_HR():  
    def __init__(self, name, bxid, length = None):
        #All must be binary strings apart from name
        self.name      = name   
        self.bxid      = bxid   
        self.length    = length 
        self.data      = None
        self.linenum   = None
        self.zerosafter= None

        self.flag = '0'
        if self.name != 'Normal': self.flag = '1'

        if self.name == 'BxVeto':
            self.length = '010001'
        elif self.name == 'HeaderOnly':
            self.length = '010010'
        elif self.name == 'BusyEvent':
            self.length = '010011'
        elif self.name == 'BufferFull':
            self.length = '010100'
        elif self.name == 'BufferFullN':
            self.length = '010101'
        elif self.name == 'NZS':
            self.length = '000110'

        #set parity
        header  = self.bxid+self.flag+self.length
        ones    = (header).count('1')
        expected_parity = 1
        if ones%2 == 0: expected_parity = 0
        self.parity = str(expected_parity)

    #def performChecks(self):
    #    #name checks
    #    special_names  = ['Idle', 'BxVeto', 'HeaderOnly', 'BusyEvent', 'BufferFull', 'BufferFullN']
    #    list_typenames = special_names + ['Normal', 'NZS', 'Sync']
    #    if self.name not in list_typenames: raise Exception(self.name, 'not in', list_typenames)

    #    #length checks
    #    if not (len(self.bxid) != 4 or len(self.bxid) != 12): raise Exception(self.bxid, 'bxid should be 4b wide but is not')
    #    if len(self.parity) != 1: raise Exception(self.parity, 'parity should be 1b wide but is not')
    #    if len(self.flag) != 1: raise Exception(self.flag, 'flag should be 1b wide but is not')
    #    if len(self.length) != 6: raise Exception(self.length, 'length should be 6b wide but is not')

    #    #check flag to expected
    #    if self.name in special_names+['NZS'] and self.flag != 1: raise Exception('The speicial packet', self.name, 'must have flag set to 1, but is instead', self.flag)
    #    if self.name in special_names and self.data != 'NoData': raise Exception('The speicial packet', self.name, 'must have data set to NoData, but is instead', self.data)
    #    if self.name == 'NZS' and len(self.data) = 712: raise Exception('The NZS data packet length should be 712b, but is ', len(self.data))

    #    if self.name == 'Idle':
    #        if self.bxid != '0000': raise Exception()

class SyncPacket():  
    def __init__(self, name, bxid, sync_patt_fixed = None):
        #All must be binary strings apart from name
        self.name   = name   
        self.bxid   = bxid   
        self.bxid_full   = bxid
        if sync_patt_fixed is None:
            self.data   = conv_hexTobin('0xcaa', bitwidth = '12b')
        else:
            self.data   = conv_hexTobin(sync_patt_fixed, bitwidth = '12b')

        self.linenum= None
        self.zerosafter= None 
        self.header = self.bxid
        self.parity = None
        self.flag   = None
        self.length = None

    #def performChecks(self):
    #    #name check
    #    if self.name == 'Sync': raise Exception(self.name, 'not Sync')
    #    #length checks
    #    if len(self.bxid) != 12: raise Exception(self.bxid, 'bxid should be 4b wide but is not')
    #    if len(self.data)%12 == 0: raise Exception(self.data, 'length should be multiple of 12b but is not')
    #    #sync check
    #    if self.data == conv_hexTobin('caa', bitwidth = '12b'): raise Exception(self.data, 'should be', conv_hexTobin('caa', bitwidth = '12b'), 'but is', self.data)

def conv_binTonum(binr):
    return int(binr,2)

def conv_numTobin(num, bitwidth, fillstring='0') : #num is int, bitwidth = '12b' (string), fillstring (string) use typesetting
    return format(num, fillstring+bitwidth)

def conv_hexTobin(my_hexdata, scale = 16, bitwidth = '12b', padright = False): #scale = 16 for hexadecimal
    my_hexdata = my_hexdata.replace('0x','')
    num    = int('1'+my_hexdata, scale)
    binnum = bin(num)[3:]
    zeros_needed = (int(bitwidth.replace('b','')) - len(binnum)) * '0'
    if padright: 
        return binnum+zeros_needed
    else:
        return zeros_needed+binnum

def conv_binTohex(binr):
    #return hex(conv_binTonum(binr))
    return '0x{:0{}x}'.format(int(binr, 2), len(binr)//4)

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        pass
        #raise argparse.ArgumentTypeError('Boolean value expected.')


# ---------------- Utilities to read Questa files -------------------------------------------------------------------------------------


# Input: filename of the event list Questa file (!! NOT TABULAR LIST!!)
#        t0: moment when we start saving data
# Output: a dictionary of dictionaries.
#   example: mysignal['/top/tb/.../clk'][1234000]
#   will give you the value of signal /.../clk at time 1234000
def read_questa(Filename, t0):
    mysignal = {}
    with open(Filename, 'r') as myfile:
        line = myfile.readline().strip('\n') #This line is time = 0, ignore
        line = myfile.readline().strip('\n') #This is the first name
        while (line[0]!='@'): #We fill in the names
            [name, value] = line.split()
            mysignal[name]={}
            mysignal[name][0]=value
#            print('Signal: ', name)
            line = myfile.readline().strip('\n')
        while (line!=''): #We read the actual data
            if (line[0]=='@'): #It is a time
                [moment_txt, delta] = line.split()
                moment=int(moment_txt.strip('@'))
            else: #It is a signal change
                if moment >= t0 :
                    [name, value_tmp] = line.split()
                    if 'h' in value_tmp:
                        [base, value] = value_tmp.split('h')
                    else:
                        value=value_tmp

                    mysignal[name][moment]=value
            line = myfile.readline().strip('\n')
    return mysignal

# Inputs:
#   thisSignal: a dictionary of {time: value}, for instance mySignal['/.../clk']
#   moment: an integer time, doesn't need to be in the dictionary
# Output:
#   The value of the signal at the specified time, extrapolated from the changes
# Example:
#   findSignalValue(mySignal['/.../clk'], 12345000) will give you the value of clk at that time

def findSignalValue(thisSignal, moment):
    changeMoments = thisSignal.keys()
    previousChange = 0
    for x in changeMoments:
        if x == moment :
            return thisSignal[x]
        elif x > moment :
            return thisSignal[previousChange]
        else :
            previousChange = x
    return thisSignal[previousChange]

# Inputs:
#   sampledSignal: what signal to sample, [time, value]
#   clockSignal: what signal triggers the sampling (clk), [time, value]
#   enableSignal: what signal enables the sampling, [time, value]
#           -> 'none' disables this feature and will always sample
#
# Output:
#   a list of (time, value) of the sampled signal
#

def sampleSignal(sampledSignal, clockSignal, enableSignal):
    sampledSignalList = {}
    for clkCycle in clockSignal:
        if clockSignal[clkCycle] == '1' :
            if (enableSignal == 'none' or findSignalValue(enableSignal, clkCycle) == '1'):
                sampledValue = findSignalValue(sampledSignal, clkCycle)
                sampledSignalList[clkCycle] = sampledValue
    return sampledSignalList

def associateBXID(BXID, Signal):
    BXID_cnt = 0
    associatedSignals = {}
    last_bxid = 0
    for s in Signal:
        timeStamp=s
        values=Signal[s]
        bxid=findSignalValue(BXID, timeStamp)  # hex
        #if bxid == 0xDEB:
        if int(bxid, base=16) < last_bxid:
            BXID_cnt += 1
        bxid_extended = str(BXID_cnt) + bxid
        if bxid_extended in associatedSignals:
            associatedSignals[bxid_extended]+= [values]
        else:
            associatedSignals[bxid_extended]=[values]
        
        last_bxid=int(bxid, base=16)
    return associatedSignals


# ---------------- Utilities to summarize CSV files -------------------------------------------------------------------------------------
def read_csv(Filename):
    readouts = []
    print_sync_info = False
    with open(Filename, 'r') as myfile:
        #print('Reading file ' + Filename)
        #print('Skipping the header')
        _ = [myfile.readline() for x in range(1)]
        line = myfile.readline().strip('\n')
        while(line != ''):
            lsplit = line.split(',')
            lsplit = lsplit[:-1] + [(lsplit[-1]).replace('\r', '')]
            lsplit = [ls.replace(' ', '') for ls in lsplit]
            #print(lsplit)
    
            linenum = int(lsplit[0])
            zerosafter = int(lsplit[1])
            name = lsplit[2]
            dtp = None
            if name != 'Idle':
                bxid = conv_hexTobin(lsplit[3] , bitwidth = '4b')
                if len(bxid) != 4: 
                    #print('Bxid is trimmed!')
                    bxid = bxid[-4:] #caution

                if name == 'Sync':
                    bxid_sync = conv_hexTobin(lsplit[3] , bitwidth = '12b')
                    if len(bxid_sync) != 12: raise Exception('Bxid sync is not of length 12b. Check!')

                    sync_patt_fixed = 'fff' #'caa' 
                    if not print_sync_info:
                        #print('Note the sync pattern being used is: fff')
                        print_sync_info = True

                    dtp = SyncPacket(name, bxid_sync, sync_patt_fixed = sync_patt_fixed)
                elif name == 'Normal':
                    length  = conv_numTobin(int(lsplit[8]), '6b')
                    if len(length) != 6: raise Exception('Length is not of length 6b. Check!')

                    dtp     = DataPacket_HR(name, bxid, length)
                    hitdata = ''
                    if conv_binTonum(length) != 0:
                        if 'IncompleteData' not in lsplit[9]:
                            for htd in lsplit[9:]:
                                hd = htd.split('(')[-1]
                                if '(' in htd: #if there is (stripid:adcval) format use that
                                    #print(hd)
                                    strippedhd = ((hd.replace('(','')).replace(')','')).split(':')
                                    sid = strippedhd[0]
                                    adc = strippedhd[1]
                                    #print(sid, adc)
                                    if len(conv_numTobin(int(sid), '7b')) != 7 or len(conv_numTobin(int(adc), '5b')) != 5:
                                        raise Exception('The ASICID and ADC are not of correct width i.e. 7b and 5b, respectively. Check!')

                                    hitdata += conv_numTobin(int(sid), '7b')+conv_numTobin(int(adc), '5b')
                                else: #or else use hex
                                    hd       = htd.split('(')[0]
                                    if len(conv_hexTobin(hd, bitwidth = '12b')) != 12:
                                        raise Exception('The hit data is not of length 12b in normal packet. Check!')

                                    hitdata += conv_hexTobin(hd, bitwidth = '12b')
                        else:
                            print('INFO: The length of the normal packet is {0}, but data packet says NoData. Maybe the data has overflown!.'.format(conv_binTonum(length)))
                            hitdata = (lsplit[9].split('(')[1]).split(')')[0]
    
                    dtp.data = hitdata
                    #print(hitdata)
                elif name == 'NZS':
                    dtp  = DataPacket_HR(name, bxid)
                    hitdata = ''
                    if 'IncompleteData' not in lsplit[9]:
                        if len(conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)) != 24 + 6 * 128:
                            raise Exception('The hit data for NZS packet is not of correct lenth. Check!')

                        hitdata  = conv_hexTobin(lsplit[9], bitwidth = str(24 + 6 * 128)+'b', padright = True)

                        if len(hitdata) > 24 + 6 * 128:
                            print('INFO: Truncating the NZS data packet to fit to', 24 + 6 * 128, '. If you did not intend this check!')
                            hitdata = hitdata[:24 + 6 * 128]
                    else:
                        print('INFO: There is overflow in NZS data!')
                        hitdata = (lsplit[9].split('(')[1]).split(')')[0]
    
                    dtp.data = hitdata
                    #print(hitdata)
                else:
                    dtp  = DataPacket_HR(name, bxid)
            else:
                dtp = DataPacket('Idle', '0000', '1', '1', '110000')

            readouts += [dtp]
            line = myfile.readline().strip('\n')

    return readouts

def write_csv(Newfilename, asicheaders, readouts):
    print('Writing a new file', Newfilename)
    with open(Newfilename, 'w') as csvfile: 
        #print(asicheaders)
        csvfile.write(asicheaders)
        for readout in readouts:
            BXIDHex     =readout.bxid
            Packets     =readout.names
            Lengths     =readout.lengths
            TotalLength =readout.totallengths
            Hits        =readout.data
            s = '{0:},{1:},{2:},{3:},{4:},\n'.format(BXIDHex,Packets,Lengths,TotalLength,Hits)
            #print(s)
            csvfile.write(s)

def deduce_BXID(readouts):
    currentBXID = 0x100 #By default it is always 0x100
    for event in readouts:
        if event.name == 'Sync':
            currentBXID=conv_binTonum(event.bxid)+1
        elif event.name != 'Idle':
            event.bxid=currentBXID
            currentBXID+=1
            currentBXID%=3564
    return readouts

def cleanup_readouts(readouts):
    for event in readouts:
        if event.name =='HeaderOnly':
            event.name = 'HO'
            event.data = '0'
        if event.data==None:
            event.data = '0'
        if event.data=='':
            event.data = '0'
        if event.name=='Normal':
            event.name='NP'
    return readouts

def remove_packets(readouts, packet_name):
    return [x for x in readouts if x.name !=packet_name]

def make_data_hex(readouts):
    for event in readouts:
        event.data = conv_binTohex(event.data)[2:]
    return readouts

def format_properly(readouts):
    temp = deduce_BXID(readouts)
    temp = cleanup_readouts(temp)
    temp = remove_packets(temp, 'Sync')
    temp = remove_packets(temp, 'Idle')
    temp = make_data_hex(temp)
    return temp

def make_lane_dictionary(Lane_readouts):
    # We compose a dictionary with the lane information
    lane = {}
    orbit = 0
    last_bxid = 0

    for event_num in range (0, len(Lane_readouts[0])):
        # We check all BXIDs coincide
        for ASIC in range(0,3):
            if Lane_readouts[ASIC][event_num].bxid != Lane_readouts[ASIC+1][event_num].bxid :
                print('BXID error between ASICs ' + ASIC + 'and ' + (ASIC+1))
                exit(-1)

        # We compute the extended BXID (including orbit number)
        bxid_dec = Lane_readouts[0][event_num].bxid
        bxid_hex = hex(bxid_dec)[2:]
        bxid_hex = '00'+bxid_hex
        bxid_hex = bxid_hex[-3:]
        if last_bxid > bxid_dec:
            orbit = orbit +1
        last_bxid = bxid_dec
        bxid_ext = hex(orbit)[2:] + bxid_hex

        # We create a list with the events:
        lane_with_chipID = []
        for ASIC in range(0,4):
            asic_data=Lane_readouts[ASIC][event_num].data
            if asic_data!='0': # We add the elements with the ASIC ID in front
                asic_data = asic_data.upper()
                lane_with_chipID.extend([str(ASIC) + asic_data[i:i+3] for i in range (0, len(asic_data), 3)])

        # We add the hits to our dictionary (!extended BXID in hex!)
        bxid_ext = bxid_ext.upper()
        lane[bxid_ext]=lane_with_chipID
    
    return lane

def slice_signal(signal, original_width, start_bit, desired_width):
    temp ={}
    for event_time in signal:
        event_bin = "{0:08b}".format(int(signal[event_time], 16))
        event_bin = event_bin.zfill(original_width) #We add padding if needed
        event_bin = event_bin[::-1] #We invert the string to make it easier to compute
        slice_bin = event_bin[start_bit:start_bit+desired_width] 
        slice_bin = slice_bin[::-1] #We invert back to little endian
        temp[event_time]= conv_binTohex(slice_bin)[2:].upper()
    return temp

class SummaryFile():
    def __init__(self, names, bxid, lengths, totallengths, hits):
        self.names        = names   
        self.bxid         = bxid   
        self.lengths      = lengths
        self.totallengths = totallengths
        self.data         = hits

# Binary search algorithm
# Returns the smallest value in the list greater than the target
def binary_search(sorted_list, target):
    low = 0
    high = len(sorted_list) - 1
    while low <= high:
        mid = (low + high) // 2
        if sorted_list[mid] > target:
            high = mid - 1
        elif sorted_list[mid] < target:
            if mid == len(sorted_list) - 1: # We are at the end of the list and the target is not in the list
                return None
            low = mid + 1
        else:
            return sorted_list[mid]
    return sorted_list[low]
